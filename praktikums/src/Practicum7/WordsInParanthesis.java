package Practicum7;

import lib.TextIO;

public class WordsInParanthesis {
    public static void main(String[] args) {
        System.out.println("Enter a line of text: ");
        String line = TextIO.getlnString();
        String[] words = line.split(" ");
        for (String word : words) {
            System.out.print("(" + word + ") ");

        }

    }

}
