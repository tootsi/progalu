package Practicum7;

/**
 * Created by Ats on 14.11.2016.
 */
public class MethodVsVariable {
    public static void main(String[] args) {

        int y = 5;
        Integer z = 2;
        y = add2(y);
        System.out.println(y);

    }

    private static int add2(int x) {
        return x + 3;
    }

}
