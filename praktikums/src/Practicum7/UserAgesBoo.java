package Practicum7;

import lib.TextIO;

import java.util.ArrayList;

/**
 * Created by Ats on 14.11.2016.
 */
public class UserAgesBoo {

    public static void main(String[] args) {

        ArrayList<Human> people = new ArrayList<>();

        while (true) {
            System.out.println("Enter a name: ");
            String name = TextIO.getlnString();
            if (name.isEmpty()) {
                break;
            }
            System.out.println("Enter an age: ");
            int age = TextIO.getlnInt();


            Human newHuman = new Human(name, age);
            people.add(newHuman);
        }

        for (Human human : people) {
            human.greet();
        }


    }

/*    public static void NumberOfAsInNames() {
        ArrayList<String> names = new ArrayList<>();

        //since we don't know how many times the user will enter names, the loop will have to be "while"

        boolean isEmpty = false;
        while (!isEmpty) {
            System.out.println("Enter a name: ");
            String name = TextIO.getln();
            if (name.isEmpty()) {
                isEmpty = true;
            } else {
                names.add(name);
            }
        }

    }*/
}
