package Practicum7;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Ats on 14.11.2016.
 */
public class BankAccountTest {

    @Test
    public void whenOpeningABankaccountMoneyAmmountIsZero() {

        BankAccount account = new BankAccount();
        Assert.assertEquals(0., account.getBalance(), 0.0001);

    }

    @Test

    public void whenDepositingMoneyBalanceIsIncreased() {

        BankAccount account = new BankAccount();
        double amountOfMoney = 10;
        account.deposit(amountOfMoney);
        Assert.assertEquals(10. , account.getBalance(), 0.001);


    }

}