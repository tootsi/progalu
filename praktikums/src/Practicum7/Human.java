package Practicum7;

/**
 * Created by Ats on 14.11.2016.
 */
public class Human {

    private String name;
    private int age;

    //Excercise 3 from practicum 7:

    public Human() {

    }

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greet() {
        System.out.println(greetAsString());
    }

    public String greetAsString() {

        if (age < 3) {
            return "Boo boo";
        }
        return "Hello, I am " + name + " and I am " + age + " years old.";

        //TODO: Implement this method
        //return null;
    }

    @Override
    public String toString() {
        return name + ", " + age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
