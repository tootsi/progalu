package Practicum7;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class HumanTest {
    @Test
    public void greetAdult() throws Exception {

        Human myHuman = new Human("John", 24);

        Assert.assertEquals("Hello, I am John and I am 24 years old.", myHuman.greetAsString());


    }

    public void greetChild() throws Exception {

        Human myHuman = new Human("James", 2);

        Assert.assertEquals("Boo Boo", myHuman.greetAsString());

    }

}