package Practicum7;

import lib.TextIO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class NameAndResult {
    public static void main(String[] args) {
        ArrayList<Athlete> athletes = new ArrayList<>();
        while(true) {
            System.out.println("Insert the name and result of the athlete: ");
            String line = TextIO.getlnString();
            if (line.isEmpty()) {
                break;
            }

            String[] words = line.split(" ");
            String name = words[0];
            String resultAsString = words[1];
            double result = Double.parseDouble(resultAsString); //entering "name name" will produce error, this needs to be resolved here

            Athlete newAthlete = new Athlete(name, result);
            athletes.add(newAthlete);

        }

        Collections.sort(athletes, new Comparator<Athlete>() {
            @Override
            public int compare(Athlete athlete1, Athlete athlete2) {
                return athlete2.getResult().compareTo(athlete1.getResult());
            }
        });

        athletes.get(0).greet();

        for (Athlete athlete : athletes) {
            System.out.println(athlete.getName() + " " + athlete.getResult());
        }


    }
}
