package praktikum2;

import lib.TextIO;

/**
 * Created by Ats on 11.09.2016.
 */
public class Product {
    public static void main(String[] args) {
        System.out.println("Please enter the first number:");
        int firstNumber = TextIO.getlnInt();
        System.out.println("Please enter the second number:");
        int secondNumber = TextIO.getlnInt();
        int product = firstNumber * secondNumber;
        System.out.println("The product of the numbers " + firstNumber + " and " + secondNumber + " is: " + product);
    }
}
