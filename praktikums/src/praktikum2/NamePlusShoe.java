package praktikum2;

import lib.TextIO;

/**
 * Created by Ats on 11.09.2016.
 */
public class NamePlusShoe {
    public static void main(String[] args) {
        System.out.print("Insert a name: ");
        String name = TextIO.getlnString();
        System.out.print("Insert a shoe size: ");
        String shoe = TextIO.getlnString();

        System.out.println(name + " jalanumber on " + shoe);
    }
}
