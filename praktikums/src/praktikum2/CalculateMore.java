package praktikum2;

/**
 * Created by Ats on 11.09.2016.
 */
public class CalculateMore {
    public static void main(String[] args) {
        double a = -15.3;
        double b = 20.2;
        double c;

        c = a * b;

        System.out.println(a + " + " + b + " = " + c);
        System.out.format("%.1f * %.1f = %.2f", a, b, c);
    }
}
