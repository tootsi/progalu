package praktikum2;

import lib.TextIO;

/**
 * Created by Ats on 11.09.2016.
 */
public class PeopleGroup {
    public static void main(String[] args) {
        System.out.println("Please enter the number of people:");
        int people = TextIO.getlnInt();
        System.out.println("Please enter the size of the group:");
        int group = TextIO.getlnInt();
        int groups = people / group;
        int leftover = people % group;

        //int product = firstNumber * secondNumber;
        System.out.println("The number of groups created is: " + groups);
        System.out.println("The number of people left over: " + leftover);
    }
}
