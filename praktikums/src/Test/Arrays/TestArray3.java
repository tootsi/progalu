package Test.Arrays;

//Average of array

public class TestArray3 {

    public static void main(String[] args) {

        Integer[] testList = {2, 4, 6, 8, 10};

        int total = 0;
        int average;
        for (int i = 0; i < testList.length; i++) {
            total = total + testList[i];
        }

        average = total / testList.length;
        System.out.println(average);

    }

}
