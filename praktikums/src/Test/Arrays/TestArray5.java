package Test.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class TestArray5 {

    public static void main(String[] args) {
        
        String[] thisArray = {"this", "that", "them"};

        for (String name : thisArray) { //A for-each loop for printing all the values in the Array
            System.out.println(name);
        }
        
    }
    
}
