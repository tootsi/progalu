package Test.Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ats on 24.01.2017.
 */
public class RemoveFromArray {

    public static void main(String[] args) {

        String[] str = {"a", "bb", "ccc", "dddd"};
        System.out.println(Arrays.toString(str));

        List<String> list = new ArrayList<String>(Arrays.asList(str));
        list.removeAll(Arrays.asList("a"));
        str = list.toArray(str);

        System.out.println(Arrays.toString(str));


    }

}
