package Test.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class NumberAverage {

    public static void main(String[] args) {

        double[] list = {1, 2, 3, 4.9001, 5};

        System.out.println(average(list));

    }

    public static double average ( double... numbers ) {

        double sum;
        double average;
        sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }

        average = sum / numbers.length;

        return average;

    }

}
