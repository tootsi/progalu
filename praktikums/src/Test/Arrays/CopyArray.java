package Test.Arrays;

import java.util.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class CopyArray {

    public static void main(String[] args) {
        String[] words = {"one", "two", "three", "four", "mikk"};
        String[] B;
        B = Arrays.copyOf( words, 2 );
        System.out.println(Arrays.toString(B));

    }
}

///playerList = Arrays.copyOf( playerList, 2*playerList.length );
//could be used to double the amount of space available in a partially full array with just one line of code. We can also use Arrays.copyOf to decrease the size of a partially full array. We might want to do that to avoid having a lot of excess, unused spaces. To implement this idea, the code for deleting player number k from the list of players might become
