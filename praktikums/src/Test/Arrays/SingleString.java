package Test.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class SingleString {

    public static void main(String[] args) {

        String[] listofstrings = {"üks", "kaks", "kolm", "neli", "viis"};

        System.out.println(concat(listofstrings));

    }

    public static String concat (String... values) {

        StringBuffer buffer; // Use a StringBuffer for more efficient concatenation.
        buffer = new StringBuffer(); // Start with an empty buffer.
        for (String item : values) { // A "for each" loop for processing the values.

            buffer.append(item); // Add string to the buffer.

        }

        return buffer.toString(); // return the contents of the buffer
    }

}
