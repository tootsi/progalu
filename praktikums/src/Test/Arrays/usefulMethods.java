package Test.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class usefulMethods {
}

/*
*
*Arrays.fill( array, value ) -- Fill an entire array with a specified value. The type of value must be compatible with
* the base type of the array. For example, assuming that numlist is an array of type double[], then Arrays.fill(numlist,17)
* will set every element of numlist to have the value 17.

Arrays.fill( array, fromIndex, toIndex, value ) -- Fills part of the array with value, starting at index number fromIndex
and ending with index number toIndex-1. Note that toIndex itself is not included.

Arrays.toString( array ) -- A function that returns a String containing all the values from array, separated by commas and
enclosed between square brackets. The values in the array are converted into strings in the same way they would be if they
were printed out.

Arrays.sort( array ) -- Sorts the entire array. To sort an array means to rearrange the values in the array so that they
are in increasing order. This method works for arrays of String and arrays of primitive type values (except for boolean,
which would be kind of silly). But it does not work for all arrays, since it must be meaningful to compare any two values
in the array, to see which is "smaller." We will discuss array-sorting algorithms in Section 7.4.

Arrays.sort( array, fromIndex, toIndex ) -- Sorts just the elements from array[fromIndex] up to array[toIndex-1]

Arrays.binarySearch( array, value ) -- Searches for value in the array. The array must already be sorted into increasing
order. This is a function that returns an int. If the value is found in the array, the return value is the index of an
element that contains that value. If the value does not occur in the array, the return value is -1. We will discuss the
binary search algorithm in Section 7.4.
*
*
*
*
* */
