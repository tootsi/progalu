package Test.Arrays;

/**
 * Created by Ats on 4.12.2016.
 */
public class TestArray2 {
    public static void main(String[] args) {

        Integer[] testList = {6, 1, 2, 3, 4, 5};

        for (int i = 0; i < testList.length; i++) { //printing out array
            System.out.println(testList[i]);
        }

        int total = 0; //Summing up array values
        for (int i = 0; i < testList.length; i++) {
            total += testList[i];
        }
        System.out.println(total);

        int largest = testList[0]; //finding the biggest element;
        for (int i = 0; i < testList.length; i++) {
            if (testList[i] > largest) largest = testList[i];
        }
        System.out.println(largest);


    }
}
