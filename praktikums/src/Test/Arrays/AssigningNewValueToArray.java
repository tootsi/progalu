package Test.Arrays;

import lib.TextIO;

import java.util.Arrays;

public class AssigningNewValueToArray {

    public static void main(String[] args) {

        int[] num = {1, 2, 3, 4, 5};
        System.out.println("Old:");
        System.out.println(Arrays.toString(num));

        num = new int[] {5, 4, 3, 2, 1};
        System.out.println("New");
        System.out.println(Arrays.toString(num));


/*        System.out.println("New:");
        System.out.println();

        public static Integer newarray(Integer... values);
        int[] numbers;
        numbers = new int[5];
        for (int i = 0; i < numbers.length; i++) {
            int j;
            System.out.println("Insert 5 numbers: ");
            j = TextIO.getInt();
            numbers.;
        }

        System.out.println("Old array is: ");
        System.out.print(Arrays.toString(numbers));*/


    }

}
