package Test.Arrays;

import java.util.Scanner;

/**
 * Created by Ats on 24.01.2017.
 */
public class RemovingIntArray {

    public static void main(String[] args) {

        Integer[] sourceAry = {1,2,3,4,5,6,7,8,9,10}; // 10 element int array
        Integer[] destAry = new Integer[sourceAry.length-1]; // 9 element int array
        System.out.print("There are " + sourceAry.length + " elements. Enter the position or index (0 to 9) to remove: ");
        Scanner scan = new Scanner(System.in);
        int position = scan.nextInt(); // get input
        for (int i=0; i < sourceAry.length; i++) {
            if (i == position) {
                sourceAry[i] = null; // null the source
            } else {
                destAry[i] = sourceAry[i]; // put in destination
            }
        }
// output
        for (int i=0; i < destAry.length; i++) {
            System.out.print(destAry[i] + " ");
        }

    }

}
