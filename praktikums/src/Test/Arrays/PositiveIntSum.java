package Test.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class PositiveIntSum {

    public static void main(String[] args) {

        int[] integers = {2, 3, 4, 5, 6, 1, -2, 9, 11, -100, 8};

        int sum = 0;
        for (int item : integers) {
            if (item > 0) {
                sum = sum + item;
            }
        }
        System.out.println(sum);

    }

}
