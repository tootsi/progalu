package Test.Arrays;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Ats on 24.01.2017.
 */
public class Duplicate {

    public static void main(String[] args) {
        String[] test = {"a", "b", "abvc", "abccc", "a", "bbc", "ccc", "abc", "bbc"};
        String[] deduped = removeDuplicate(test);
        print(deduped);
    }

    public static String[] removeDuplicate(String[] words) {
        Set<String> wordSet = new LinkedHashSet<String>();
        for (String word : words) {
            wordSet.add(word);
        }
        return wordSet.toArray(new String[wordSet.size()]);
    }

    public static void print(String[] words) {
        for (String word : words) {
            System.out.println(word);
        }
    }
}
