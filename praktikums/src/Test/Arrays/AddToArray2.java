package Test.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class AddToArray2 {

    public static void main(String[] args) {
        int[] series = new int[0];
        int x = 5;


        series = addInt(series, x);

        //print out the array with commas as delimiters
        System.out.print("New series: ");
        for (int i = 0; i < series.length; i++){
            if (i == series.length - 1){
                System.out.println(series[i]);
            }
            else{
                System.out.print(series[i] + ", ");
            }
        }
    }

    public static int[] addInt(int [] series, int newInt){
        //create a new array with extra index
        int[] newSeries = new int[series.length + 1];

        //copy the integers from series to newSeries
        for (int i = 0; i < series.length; i++){
            newSeries[i] = series[i];
        }
//add the new integer to the last index
        newSeries[newSeries.length - 1] = newInt;



        return newSeries;

    }

}
