package Test.Arrays;

import java.util.Arrays;

/**
 * Created by Ats on 24.01.2017.
 */
public class AddToArray {

    public static void main(String[] args) {
        int[] series = {4, 2};
        series = addElement(series, 3);
        series = addElement(series, 1);

        System.out.println(Arrays.toString(series));
    }

    static int[] addElement(int[] a, int e) {
        a = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }
}
