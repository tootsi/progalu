
/*package Test.Muu.bkk.;*/

/**
 * Created by Ats on 24.01.2017.
 *//*

public class TestBook {
}

/*
Section 1
        A variable is just a memory location (or several consecutive locations treated as a unit) that has been given a name so that it can be easily referred to and used in a program.
        Control structures are special instructions that can change the flow of control. There are two basic types of control structure: loops, which allow a sequence of instructions to be repeated over and over, and branches, which allow the computer to decide between two or more different courses of action by testing conditions that occur as the program is running.
        The central concept of object-oriented programming is the object, which is a kind of module containing data and subroutines. The point-of-view in OOP is that an object is a kind of self-sufficient entity that has an internal state (the data it contains) and that can respond to messages (calls to its subroutines). A mailing list object, for example, has a state consisting of a list of names and addresses. If you send it a message telling it to add a name, it will respond by modifying its state to reflect the change. If you send it a message telling it to print itself, it will respond by printing out its list of names and addresses.
        Now, Java actually has two complete sets of GUI components. One of these, the AWT or Abstract Windowing Toolkit, was available in the original version of Java. The other, which is known as Swing, was introduced in Java version 1.2, and is used in preference to the AWT in most modern Java programs. The program that is shown above uses components that are part of Swing.
        Section 2

        2.2.2
        There are eight so-called primitive types built into Java. The primitive types are named byte, short, int, long, float, double, char, and boolean.
        •	short corresponds to two bytes (16 bits). Variables of type short have values in the range -32768 to 32767.
        •	int corresponds to four bytes (32 bits). Variables of type int have values in the range -2147483648 to 2147483647.
        •	long corresponds to eight bytes (64 bits). Variables of type long have values in the range -9223372036854775808 to 9223372036854775807.
        The float data type is represented in four bytes of memory, using a standard method for encoding real numbers. The maximum value for a float is about 10 raised to the power 38. A float can have about 7 significant digits. (So that 32.3989231134 and 32.3989234399 would both have to be rounded off to about 32.398923 in order to be stored in a variable of type float.) A double takes up 8 bytes, can range up to about 10 to the power 308, and has about 15 significant digits. Ordinarily, you should stick to the double type for real values.
        A data value is stored in the computer as a sequence of bits. In the computer's memory, it doesn't look anything like a value written on this page. You need a way to include constant values in the programs that you write. In a program, you represent constant values as literals. A literal is something that you can type in a program to represent a value. It is a kind of name for a constant value.
        For example, to type a value of type char in a program, you must surround it with a pair of single quote marks, such as 'A', '*', or 'x'. The character and the quote marks make up a literal of type char. Without the quotes, A would be an identifier and * would be a multiplication operator. The quotes are not part of the value and are not stored in the variable; they are just a convention for naming a particular character constant in a program. If you want to store the character A in a variable ch of type char, you could do so with the assignment statement
        ch = 'A';

        Certain special characters have special literals that use a backslash, \, as an "escape character". In particular, a tab is represented as '\t', a carriage return as '\r', a linefeed as '\n', the single quote character as '\'', and the backslash itself as '\\'. Note that even though you type two characters between the quotes in '\t', the value represented by this literal is a single tab character.
        Numeric literals are a little more complicated than you might expect. Of course, there are the obvious literals such as 317 and 17.42. But there are other possibilities for expressing numbers in a Java program. First of all, real numbers can be represented in an exponential form such as 1.3e12 or 12.3737e-108. The "e12" and "e-108" represent powers of 10, so that 1.3e12 means 1.3 times 1012 and 12.3737e-108 means 12.3737 times 10-108. This format can be used to express very large and very small numbers. Any numeric literal that contains a decimal point or exponential is a literal of type double. To make a literal of type float, you have to append an "F" or "f" to the end of the number. For example, "1.2F" stands for 1.2 considered as a value of type float. (Occasionally, you need to know this because the rules of Java say that you can't assign a value of type double to a variable of type float, so you might be confronted with a ridiculous-seeming error message if you try to do something like "x = 1.2;" if x is a variable of type float. You have to say "x = 1.2F;". This is one reason why I advise sticking to type double for real numbers.)
        Java has other types in addition to the primitive types, but all the other types represent objects rather than "primitive" data values. For the most part, we are not concerned with objects for the time being. However, there is one predefined object type that is very important: the type String. (String is a type, but not a primitive type; it is in fact the name of a class, and we will return to that aspect of strings in the next section.)
        A value of type String is a sequence of characters. You've already seen a string literal: "Hello World!". The double quotes are part of the literal; they have to be typed in the program. However, they are not part of the actual Stringvalue, which consists of just the characters between the quotes. A string can contain any number of characters, even zero. A string with no characters is called the empty string and is represented by the literal "", a pair of double quote marks with nothing between them. Remember the difference between single quotes and double quotes! Single quotes are used for char literals and double quotes for String literals! There is a big difference between the String "A" and the char 'A'.
        Within a string literal, special characters can be represented using the backslash notation. Within this context, the double quote is itself a special character. For example, to represent the string value
        I said, "Are you listening!"
        with a linefeed at the end, you would have to type the string literal:
        "I said, \"Are you listening!\"\n"
        You can also use \t, \r, \\, and Unicode sequences such as \u00E9 to represent other special characters in string literals.
        The variable-name-or-names can be a single variable name or a list of variable names separated by commas. (We'll see later that variable declaration statements can actually be somewhat more complicated than this.) Good programming style is to declare only one variable in a declaration statement, unless the variables are closely related in some way. For example:
        int numberOfStudents;
        String name;
        double x, y;
        boolean isFinished;
        char firstInitial, middleInitial, lastInitial;
        It is also good style to include a comment with each variable declaration to explain its purpose in the program, or to give other information that might be useful to a human reader. For example:
        double principal;    // Amount of money invested.
        double interestRate; // Rate as a decimal, not percentage.

        The boolean operator "not" is a unary operator. In Java, it is indicated by ! and is written in front of its single operand. For example, if test is a boolean variable, then
        test = ! test;
        will reverse the value of test, changing it from true to false, or from false to true.
        Any good programming language has some nifty little features that aren't really necessary but that let you feel cool when you use them. Java has the conditional operator. It's a ternary operator -- that is, it has three operands -- and it comes in two pieces, ? and :, that have to be used together. It takes the form
        boolean-expression ? expression1 : expression2
        The computer tests the value of boolean-expression. If the value is true, it evaluates expression1; otherwise, it evaluates expression2. For example:
        next = (N % 2 == 0) ? (N/2) : (3*N+1);
        will assign the value N/2 to next if N is even (that is, if N % 2 == 0 is true), and it will assign the value (3*N+1) to next if N is odd. (The parentheses in this example are not required, but they do make the expression easier to read.)
        In some cases, you might want to force a conversion that wouldn't be done automatically. For this, you can use what is called a type cast. A type cast is indicated by putting a type name, in parentheses, in front of the value you want to convert. For example,
        int A;
        short B;
        A = 17;
        B = (short)A;  // OK; A is explicitly type cast
        //      to a value of type short




        Secion 2.4
        j = TextIO.getlnInt();     // Reads a value of type int.
        y = TextIO.getlnDouble();  // Reads a value of type double.
        a = TextIO.getlnBoolean(); // Reads a value of type boolean.
        c = TextIO.getlnChar();    // Reads a value of type char.
        w = TextIO.getlnWord();    // Reads one "word" as a value of type String.
        s = TextIO.getln();        // Reads an entire input line as a String.
        TextIO includes output functions TextIO.put, TextIO.putln, and TextIO.putf.
        TextIO.writeFile("result.txt");
        In many cases, you want to let the user select the file that will be used for output. You could ask the user to type in the file name, but that is error-prone, and users are more familiar with selecting a file from a file dialog box. The statement: TextIO.writeUserSelectedFile();
        You can use the statement TextIO.readFile("data.txt") to read from a file named "data.txt" instead, or you can let the user select the input file with a GUI-style dialog box by saying TextIO.readUserSelectedFile(). After you have done this, any input will come from the file instead of being typed by the user. You can go back to reading the user's input with TextIO.readStandardInput().
        j = TextIO.getInt();     // Reads a value of type int.
        y = TextIO.getDouble();  // Reads a value of type double.
        a = TextIO.getBoolean(); // Reads a value of type boolean.
        c = TextIO.getChar();    // Reads a value of type char.
        w = TextIO.getWord();    // Reads one "word" as a value of type String.
        The names of these functions start with "get" instead of "getln". "Getln" is short for "get line" and should remind you that the functions whose names begin with "getln" will consume an entire line of data. A function without the "ln" will read an input value in the same way, but will then save the rest of the input line in a chunk of internal memory called the input buffer.
        Section 2.5
        Relational operators: The relational operators in Java are: ==, !=, <, >, <=, and >=.
        Getting back to assignment statements, Java has several variations on the assignment operator, which exist to save typing. For example, "A += B" is defined to be the same as "A = A + B". Every operator in Java that applies to two operands, except for the relational operators, gives rise to a similar assignment operator. For example:
        x -= y;     // same as:   x = x - y;
        x *= y;     // same as:   x = x * y;
        x /= y;     // same as:   x = x / y;
        x %= y;     // same as:   x = x % y;
        q &&= p;    // same as:   q = q && p;  (for booleans q and p)

        Java comes with eight built-in primitive types and a large set of types that are defined by classes, such as String. But even this large collection of types is not sufficient to cover all the possible situations that a programmer might have to deal with. So, an essential part of Java, just like almost any other programming language, is the ability to create new types. For the most part, this is done by defining new classes; you will learn how to do that in Chapter 5. But we will look here at one particular case: the ability to define enums (short for enumerated types).
        Technically, an enum is considered to be a special kind of class, but that is not important for now. In this section, we will look at enums in a simplified form. In practice, most uses of enums will only need the simplified form that is presented here.
        An enum is a type that has a fixed list of possible values, which is specified when the enum is created. In some ways, an enum is similar to the boolean data type, which has true and false as its only possible values. However, boolean is a primitive type, while an enum is not.
        The definition of an enum type has the (simplified) form:
enum enum-type-name { list-of-enum-values }
        This definition cannot be inside a subroutine. You can place it outside the main() routine of the program. The enum-type-name can be any simple identifier. This identifier becomes the name of the enum type, in the same way that "boolean" is the name of the boolean type and "String" is the name of the String type. Each value in the list-of-enum-values must be a simple identifier, and the identifiers in the list are separated by commas. For example, here is the definition of an enum type named Season whose values are the names of the four seasons of the year:
enum Season { SPRING, SUMMER, FALL, WINTER }
    By convention, enum values are given names that are made up of upper case letters, but that is a style guideline and not a syntax rule. An enum value is a constant; that is, it represents a fixed value that cannot be changed. The possible values of an enum type are usually referred to as enum constants.
        Note that the enum constants of type Season are considered to be "contained in" Season, which means -- following the convention that compound identifiers are used for things that are contained in other things -- the names that you actually use in your program to refer to them are Season.SPRING, Season.SUMMER,Season.FALL, and Season.WINTER.
        Once an enum type has been created, it can be used to declare variables in exactly the same ways that other types are used. For example, you can declare a variable named vacation of type Season with the statement:
        Season vacation;

        Section 3

        3.1
        The block is the simplest type of structured statement. Its purpose is simply to group a sequence of statements into a single statement. The format of a block is:
        {
        statements
        }

        That is, it consists of a sequence of statements enclosed between a pair of braces, "{" and "}". In fact, it is possible for a block to contain no statements at all; such a block is called an empty block, and can actually be useful at times. An empty block consists of nothing but an empty pair of braces. Block statements usually occur inside other statements, where their purpose is to group together several statements into a unit. However, a block can be legally used wherever a statement can occur. There is one place where a block is required: As you might have already noticed in the case of the main subroutine of a program, the definition of a subroutine is a block, since it is a sequence of statements enclosed inside a pair of braces.
        Here are two examples of blocks:
        {
        System.out.print("The answer is ");
        System.out.println(ans);
        }


        {  // This block exchanges the values of x and y
        int temp;      // A temporary variable for use in this block.
        temp = x;      // Save a copy of the value of x in temp.
        x = y;         // Copy the value of y into x.
        y = temp;      // Copy the value of temp into y.
        }
        In the second example, a variable, temp, is declared inside the block. This is perfectly legal, and it is good style to declare a variable inside a block if that variable is used nowhere else but inside the block. A variable declared inside a block is completely inaccessible and invisible from outside that block. When the computer executes the variable declaration statement, it allocates memory to hold the value of the variable. When the block ends, that memory is discarded (that is, made available for reuse). The variable is said to be local to the block. There is a general concept called the "scope" of an identifier. The scope of an identifier is the part of the program in which that identifier is valid. The scope of a variable defined inside a block is limited to that block, and more specifically to the part of the block that comes after the declaration of the variable.
        A while loop is used to repeat a given statement over and over. Of course, it's not likely that you would want to keep repeating it forever. That would be an infinite loop, which is generally a bad thing. (There is an old story about computer pioneer Grace Murray Hopper, who read instructions on a bottle of shampoo telling her to "lather, rinse, repeat." As the story goes, she claims that she tried to follow the directions, but she ran out of shampoo. (In case you don't get it, this is a joke about the way that computers mindlessly follow instructions.))
        To be more specific, a while loop will repeat a statement over and over, but only so long as a specified condition remains true. A while loop has the form:
        while (boolean-expression)
        statement
        Since the statement can be, and usually is, a block, most while loops have the form:
        while (boolean-expression) {
        statements
        }

        Suppose you have a task in mind that you want the computer to perform. One way to proceed is to write a description of the task, and take that description as an outline of the algorithm you want to develop. Then you can refine and elaborate that description, gradually adding steps and detail, until you have a complete algorithm that can be translated directly into programming language. This method is called stepwise refinement, and it is a type of top-down design. As you proceed through the stages of stepwise refinement, you can write out descriptions of your algorithm in pseudocode -- informal instructions that imitate the structure of programming languages without the complete detail and perfect syntax of actual program code.
        A more traditional approach to debugging is to insert debugging statements into your program. These are output statements that print out information about the state of the program. Typically, a debugging statement would say something like
        System.out.println("At start of while loop, N = " + N);
        Sometimes it is more convenient to test the continuation condition at the end of a loop, instead of at the beginning, as is done in the while loop. The do..while statement is very similar to the while statement, except that the word "while," along with the condition that it tests, has been moved to the end. The word "do" is added to mark the beginning of the loop. A do..while statement has the form
        do
        statement
        while ( boolean-expression );
        or, since, as usual, the statement can be a block,
        do {
        statements
        } while ( boolean-expression );
        For example, consider the following pseudocode for a game-playing program. The do loop makes sense here instead of a while loop because with the do loop, you know there will be at least one game. Also, the test that is used at the end of the loop wouldn't even make sense at the beginning:
        do {
        Play a Game
        Ask user if he wants to play another game
        Read the user's response
        } while ( the user's response is yes );
        Let's convert this into proper Java code. Since I don't want to talk about game playing at the moment, let's say that we have a class named Checkers, and that the Checkers class contains a static member subroutine named playGame() that plays one game of checkers against the user. Then, the pseudocode "Play a game" can be expressed as the subroutine call statement "Checkers.playGame();". We need a variable to store the user's response. The TextIO class makes it convenient to use a boolean variable to store the answer to a yes/no question. The input function TextIO.getlnBoolean() allows the user to enter the value as "yes" or "no" (among other acceptable responses). "Yes" is considered to be true, and "no" is considered to be false. So, the algorithm can be coded as
        boolean wantsToContinue;  // True if user wants to play again.
        do {
        Checkers.playGame();
        System.out.print("Do you want to play again? ");
        wantsToContinue = TextIO.getlnBoolean();
        } while (wantsToContinue == true);

        When the value of the boolean variable is set to false, it is a signal that the loop should end. When a booleanvariable is used in this way -- as a signal that is set in one part of the program and tested in  another part -- it is sometimes called a flag or flag variable (in the sense of a signal flag).
        By the way, a more-than-usually-pedantic programmer would sneer at the test "while (wantsToContinue == true)". This test is exactly equivalent to "while (wantsToContinue)". Testing whether "wantsToContinue == true" is true amounts to the same thing as testing whether "wantsToContinue" is true. A little less offensive is an expression of the form "flag == false", where flag is a boolean variable. The value of "flag == false" is exactly the same as the value of "!flag", where ! is the boolean negation operator. So you can write "while (!flag)" instead of "while (flag == false)", and you can write "if (!flag)" instead of "if (flag == false)".
        Although a do..while statement is sometimes more convenient than a while statement, having two kinds of loops does not make the language more powerful. Any problem that can be solved using do..while loops can also be solved using only while statements, and vice versa. In fact, if doSomething represents any block of program code, then
        do {
        doSomething
        } while ( boolean-expression );
        has exactly the same effect as
        doSomething
        while ( boolean-expression ) {
        doSomething
        }
        Similarly,
        while ( boolean-expression ) {
        doSomething
        }
        can be replaced by
        if ( boolean-expression ) {
        do {
        doSomething
        } while ( boolean-expression );
        }
        without changing the meaning of the program in any way.
        The syntax of the while and do..while loops allows you to test the continuation condition at either the beginning of a loop or at the end. Sometimes, it is more natural to have the test in the middle of the loop, or to have several tests at different places in the same loop. Java provides a general method for breaking out of the middle of any loop. It's called the break statement, which takes the form
        break;
        When the computer executes a break statement in a loop, it will immediately jump out of the loop. It then continues on to whatever follows the loop in the program. Consider for example:
        while (true) {  // looks like it will run forever!
        System.out.print("Enter a positive number: ");
        N = TextIO.getlnInt();
        if (N > 0)   // the input value is OK, so jump out of loop
        break;
        System.out.println("Your answer must be > 0.");
        }
// continue here after break

        A break statement terminates the loop that immediately encloses the break statement. It is possible to have nestedloops, where one loop statement is contained inside another. If you use a break statement inside a nested loop, it will only break out of that loop, not out of the loop that contains the nested loop. There is something called a labeled break statement that allows you to specify which loop you want to break. This is not very common, so I will go over it quickly. Labels work like this: You can put a label in front of any loop. A label consists of a simple identifier followed by a colon. For example, a while with a label might look like "mainloop: while...". Inside this loop you can use the labeled break statement "break mainloop;" to break out of the labeled loop. For example, here is a code segment that checks whether two strings, s1 and s2, have a character in common. If a common character is found, the value of the flag variable nothingInCommon is set to false, and a labeled break is used to end the processing at that point:
        boolean nothingInCommon;
        nothingInCommon = true;  // Assume s1 and s2 have no chars in common.
        int i,j;  // Variables for iterating through the chars in s1 and s2.

        i = 0;
        bigloop: while (i < s1.length()) {
        j = 0;
        while (j < s2.length()) {
        if (s1.charAt(i) == s2.charAt(j)) { // s1 and s2 have a common char.
        nothingInCommon = false;
        break bigloop;  // break out of BOTH loops
        }
        j++;  // Go on to the next char in s2.
        }
        i++;  //Go on to the next char in s1.
        }
        The continue statement is related to break, but less commonly used. A continue statement tells the computer to skip the rest of the current iteration of the loop. However, instead of jumping out of the loop altogether, it jumps back to the beginning of the loop and continues with the next iteration (including evaluating the loop's continuation condition to see whether any further iterations are required). As with break, when a continue is in a nested loop, it will continue the loop that directly contains it; a "labeled continue" can be used to continue the containing loop instead.
        The for statement makes a common type of while loop easier to write. Many while loops have the general form:
        initialization
        while ( continuation-condition ) {
        statements
        update
        }
        For example, consider this example, copied from an example in Section 3.2:
        years = 0;  // initialize the variable years
        while ( years < 5 ) {   // condition for continuing loop

        interest = principal * rate;    //
        principal += interest;          // do three statements
        System.out.println(principal);  //

        years++;   // update the value of the variable, years
        }
        This loop can be written as the following equivalent for statement:
        for ( years = 0;  years < 5;  years++ ) {
        interest = principal * rate;
        principal += interest;
        System.out.println(principal);
        }
        The initialization, continuation condition, and updating have all been combined in the first line of the for loop. This keeps everything involved in the "control" of the loop in one place, which helps make the loop easier to read and understand. The for loop is executed in exactly the same way as the original code: The initialization part is executed once, before the loop begins. The continuation condition is executed before each execution of the loop, and the loop ends when this condition is false. The update part is executed at the end of each execution of the loop, just before jumping back to check the condition.
        The formal syntax of the for statement is as follows:
        for ( initialization; continuation-condition; update )
        statement
        or, using a block statement:
        for ( initialization; continuation-condition; update ) {
        statements
        }
        Certainly, the most common type of for loop is the counting loop, where a loop control variable takes on all integer values between some minimum and some maximum value. A counting loop has the form
        for ( variable = min;  variable <= max; variable++ ) {
        statements
        }
        It's easy to count down from 10 to 1 instead of counting up. Just start with 10, decrement the loop control variable instead of incrementing it, and continue as long as the variable is greater than or equal to one.
        for ( N = 10 ;  N >= 1 ;  N-- )
        System.out.println( N );
        Now, in fact, the official syntax of a for statement actually allows both the initialization part and the update part to consist of several expressions, separated by commas. So we can even count up from 1 to 10 and count down from 10 to 1 at the same time!
        for ( i=1, j=10;  i <= 10;  i++, j-- ) {
        System.out.printf("%5d", i); // Output i in a 5-character wide column.
        System.out.printf("%5d", j); // Output j in a 5-character column
        System.out.println();       //     and end the line.
        }
        (1)   // There are 10 numbers to print.
        // Use a for loop to count 1, 2,
        // ..., 10.  The numbers we want
        // to print are 2*1, 2*2, ... 2*10.

        for (N = 1; N <= 10; N++) {
        System.out.println( 2*N );
        }


        (2)   // Use a for loop that counts
        // 2, 4, ..., 20 directly by
        // adding 2 to N each time through
        // the loop.

        for (N = 2; N <= 20; N = N + 2) {
        System.out.println( N );
        }


        (3)   // Count off all the numbers
        // 2, 3, 4, ..., 19, 20, but
        // only print out the numbers
        // that are even.

        for (N = 2; N <= 20; N++) {
        if ( N % 2 == 0 ) // is N even?
        System.out.println( N );
        }


        (4)   // Irritate the professor with
        // a solution that follows the
        // letter of this silly assignment
        // while making fun of it.

        for (N = 1; N <= 1; N++) {
        System.out.println("2 4 6 8 10 12 14 16 18 20");
        }
        Perhaps it is worth stressing one more time that a for statement, like any statement except for a variable declaration, never occurs on its own in a real program. A statement must be inside the main routine of a program or inside some other subroutine. And that subroutine must be defined inside a class. I should also remind you that every variable must be declared before it can be used, and that includes the loop control variable in a for statement. In all the examples that you have seen so far in this section, the loop control variables should be declared to be of type int. It is not required that a loop control variable be an integer. Here, for example, is a for loop in which the variable, ch, is of type char, using the fact that the ++ operator can be applied to characters as well as to numbers:
// Print out the alphabet on one line of output.
        char ch;  // The loop control variable;
        //       one of the letters to be printed.
        for ( ch = 'A';  ch <= 'Z';  ch++ )
        System.out.print(ch);
        System.out.println();

        I will finish this introduction to control structures with a somewhat technical issue that you might not fully understand the first time you encounter it. Consider the following two code segments, which seem to be entirely equivalent:
        int y;                          int y;
        if (x < 0) {                    if (x < 0) {
        y = 1;                           y = 1;
        }                               }
        else {                          if (x >= 0) {
        y = 2;                           y = 2;
        }                               }
        System.out.println(y);          System.out.println(y);
        In the version on the left, y is assigned the value 1 if x < 0 and is assigned the value 2 otherwise, that is, if x >= 0. Exactly the same is true of the version on the right. However, there is a subtle difference. In fact, the Java compiler will report an error for the System.out.println statement in the code on the right, while the code on the left is perfectly fine!
        The problem is that in the code on the right, the computer can't tell that the variable y has definitely been assigned a value. When an if statement has no else part, the statement inside the if might or might not be executed, depending on the value of the condition. The compiler can't tell whether it will be executed or not, since the condition will only be evaluated when the program is running. For the code on the right above, as far as the compiler is concerned, it is possible that neither statement, y = 1 or y = 2, will be evaluated, so it is possible that the output statement is trying to print an undefined value. The compiler considers this to be an error. The value of a variable can only be used if the compiler can verify that the variable will have been assigned a value at that point when the program is running. This is called definite assignment. (It doesn't matter that you can tell that y will always be assigned a value in this example. The question is whether the compiler can tell.)

        THE FIRST OF THE TWO BRANCHING STATEMENTS in Java is the if statement, which you have already seen in Section 3.1. It takes the form
        if (boolean-expression)
        statement-1
        else
        statement-2
        As usual, the statements inside an if statement can be blocks. The if statement represents a two-way branch. The else part of an if statement -- consisting of the word "else" and the statement that follows it -- can be omitted.
        As a final note in this section, I will mention one more type of statement in Java: the empty statement. This is a statement that consists simply of a semicolon and which tells the computer to do nothing. The existence of the empty statement makes the following legal, even though you would not ordinarily see a semicolon after a } :
        if (x < 0) {
        x = -x;
        };
        The semicolon is legal after the }, but the computer considers it to be an empty statement, not part of the if statement. Occasionally, you might find yourself using the empty statement when what you mean is, in fact, "do nothing." For example, the rather contrived if statement
        if ( done )
        ;  // Empty statement
        else
        System.out.println( "Not done yet.");
        does nothing when the boolean variable done is true, and prints out "Not done yet" when it is false. You can't just leave out the semicolon in this example, since Java syntax requires an actual statement between the if and the else. I prefer, though, to use an empty block, consisting of { and } with nothing between, for such cases.
        Occasionally, stray empty statements can cause annoying, hard-to-find errors in a program. For example, the following program segment prints out "Hello" just once, not ten times:
        for (i = 0; i < 10; i++);
        System.out.println("Hello");
        Why? Because the ";" at the end of the first line is a statement, and it is this empty statement that is executed ten times. The System.out.println statement is not really inside the for statement at all, so it is executed just once, after the for loop has completed. The for loop just does nothing, ten times!
        A switch statement allows you to test the value of an expression and, depending on that value, to jump directly to some location within the switch statement. Only expressions of certain types can be used. The value of the expression can be one of the primitive integer types int, short, or byte. It can be the primitive char type. It can be String. Or it can be an enum type (see Subsection 2.3.4 for an introduction to enums). In particular, note that the expression cannot be a double or float value.
        The positions within a switch statement to which it can jump are marked with case labels that take the form: "case constant:". The constant here is a literal of the same type as the expression in the switch. A case label marks the position the computer jumps to when the expression evaluates to the given constantvalue. As the final case in a switch statement you can, optionally, use the label "default:", which provides a default jump point that is used when the value of the expression is not listed in any case label.
        A switch statement, as it is most often used, has the form:
        switch (expression) {
        case constant-1:
        statements-1
        break;
        case constant-2:
        statements-2
        break;
        .
        .   // (more cases)
        .
        case constant-N:
        statements-N
        break;
default:  // optional default case
        statements-(N+1)
        } // end of switch statement
        The type of the expression in a switch can be an enumerated type. In that case, the constants in the case labels must be values from the enumerated type. For example, suppose that the type of the expression is the enumerated type Season defined by
enum Season { SPRING, SUMMER, FALL, WINTER }
    and that the expression in a switch statement is an expression of type Season. The constants in the case label must be chosen from among the values Season.SPRING, Season.SUMMER, Season.FALL, or Season.WINTER. However, there is a quirk in the syntax: when an enum constant is used in a case label, only the simple name, such as "SPRING" is used, not the full name, such as "Season.SPRING". Of course, the computer already knows that the value in the caselabel must belong to the enumerated type, since it can tell that from the type of expression  used, so there is really no need to specify the type name in the constant. For example, assuming that currentSeason is a variable of type Season, then we could have the switch statement:
        switch ( currentSeason ) {
        case WINTER:    // ( NOT Season.WINTER ! )
        System.out.println("December, January, February");
        break;
        case SPRING:
        System.out.println("March, April, May");
        break;
        case SUMMER:
        System.out.println("June, July, August");
        break;
        case FALL:
        System.out.println("September, October, November");
        break;
        }

        When an exception occurs, we say that the exception is "thrown". For example, we say that Integer.parseInt(str) throws an exception of type NumberFormatException when the value of str is illegal. When an exception is thrown, it is possible to "catch" the exception and prevent it from crashing the program. This is done with a try..catch statement. In simplified form, the syntax for a try..catch can be:
        try {
        statements-1
        }
        catch ( exception-class-name  variable-name ) {
        statements-2
        }
        The exception-class-name could be NumberFormatException, IllegalArgumentException, or some other exception class. When the computer executes this try..catch statement, it executes the statements in the try part. If no exception occurs during the execution of statements-1, then the computer just skips over the catch part and proceeds with the rest of the program. However, if an exception of type exception-class-name occurs during the execution of statements-1, the computer immediately jumps from the point where the exception occurs to the catch part and executes statements-2, skipping any remaining statements in statements-1. Note that only one type of exception is caught; if some other type of exception occurs during the execution of statements-1, it will crash the program as usual.
        During the execution of statements-2, the variable-name represents the exception object, so that you can, for example, print it out. The exception object contains information about the cause of the exception. This includes an error message, which will be displayed if you print out the exception object.
        A data structure consists of a number of data items chunked together so that they can be treated as a unit. An array is a data structure in which the items are arranged as a numbered sequence, so that each individual item can be referred to by its position number. In Java -- but not in other programming languages -- all the items must be of the same type, and the numbering always starts at zero. You will need to learn several new terms to talk about arrays: The number of items in an array is called the length of the array. The type of the individual items in an array is called the base type of the array. And the position number of an item in an array is called the index of that item.
        Suppose that you want to write a program that will process the names of, say, one thousand people. You will need a way to deal with all that data. Before you knew about arrays, you might have thought that the program would need a thousand variables to hold the thousand names, and if you wanted to print out all the names, you would need a thousand print statements. Clearly, that would be ridiculous! In reality, you can put all the names into an array. The array is a represented by a single variable, but it holds the entire list of names. The length of the array would be 1000, since there are 1000 individual names. The base type of the array would be String since the items in the array are strings. The first name would be at index 0 in the array, the second name at index 1, and so on, up to the thousandth name at index 999.
        The base type of an array can be any Java type, but for now, we will stick to arrays whose base type is String or one of the eight primitive types. If the base type of an array is int, it is referred to as an "array of ints." An array with base type String is referred to as an "array of Strings." However, an array is not, properly speaking, a list of integers or strings or other values. It is better thought of as a list of variables of type int, or a list of variables of type String, or of some other type. As always, there is some potential for confusion between the two uses of a variable: as a name for a memory location and as a name for the value stored in that memory location. Each position in an array acts as a variable. Each position can hold a value of a specified type (the base type of the array), just as a variable can hold a value. The value can be changed at any time, just as the value of a variable can be changed. The items in an array -- really, the individual variables that make up the array -- are more often referred to as the elements of the array.
        As I mentioned above, when you use an array in a program, you can use a variable to refer to array as a whole. But you often need to refer to the individual elements of the array. The name for an element of an array is based on the name for the array and the index number of the element. The syntax for referring to an element looks, for example, like this: namelist[7]. Here, namelist is the variable that names the array as a whole, and namelist[7] refers to the element at index 7 in that array. That is, to refer to an element of an array, you use the array name, followed by element index enclosed in square brackets. An element name of this form can be used like any other variable: You can assign a value to it, print it out, use it in an expression.
        An array also contains a kind of variable representing its length. For example, you can refer to the length of the array namelist as namelist.length. However, you cannot assign a value to namelist.length, since the length of an array cannot be changed.
        Before you can use a variable to refer to an array, that variable must be declared, and it must have a type. For an array of Strings, for example, the type for the array variable would be String[], and for an array of ints, it would be int[]. In general, an array type consists of the base type of the array followed by a pair of empty square brackets. Array types can be used to declare variables; for example,
        String[] namelist;
        int[] A;
        double[] prices;
        and variables declared in this way can refer to arrays. However, declaring a variable does not make the actual array. Like all variables, an array variable has to be assigned a value before it can be used. In this case, the value is an array. Arrays have to be created using a special syntax. (The syntax is related to the fact that arrays in Java are actually objects, but that doesn't need to concern us here.) Arrays are created with an operator named new. Here are some examples:
        namelist = new String[1000];
        A = new int[5];
        prices = new double[100];
        The general syntax is
        array-variable = new base-type[array-length];
        The length of the array can be given as either an integer or an integer-valued expression. For example, after the assignment statement "A = new int[5];", A is an array containing the five integer elements A[0], A[1], A[2], A[3], and A[4]. Also, A.length would have the value 5.
        When you create an array of int, each element of the array is automatically initialized to zero. Any array of numbers is filled with zeros when it is created. An array of boolean is filled with the value false. And an array of char is filled with the character that has Unicode code number zero.
        The arrays that we have considered so far are "one-dimensional." This means that the array consists of a sequence of elements that can be thought of as being laid out along a line. It is also possible to have two-dimensional arrays, where the elements can be laid out in a rectangular grid.
        Drawing in Java is done using a graphics context. A graphics context is an object. As an object, it can include subroutines and data. Among the subroutines in a graphics context are routines for drawing basic shapes such as lines, rectangles, ovals, and text. (When text appears on the screen, the characters have to be drawn there by the computer, just like the computer draws any other shapes.) Among the data in a graphics context are the color and font that are currently selected for drawing. (A font determines the style and size of characters.) One other piece of data in a graphics context is the "drawing surface" on which the drawing is done. Generally, the drawing surface is a rectangle on the computer screen, although it can be other surfaces such as a page to be printed. Different graphics context objects can draw to different drawing surfaces. For us, the drawing surface will be the content area of a window, not including its border or title bar.
        A graphics context is represented by a variable. The type for the variable is Graphics (just like the type for a string variable is String). The variable is often named g, but the name of the variable is of course up to the programmer. Here are a few of the subroutines that are available in a graphics context g:
        •	g.setColor(c), is called to set the color to be used for drawing. The parameter, c is an object belonging to a class named Color. There are about a dozen constants representing standard colors that can be used as the parameter in this subroutine. The standard colors include Color.BLACK, Color.WHITE, Color.LIGHT_GRAY, Color.RED, Color.GREEN, and Color.BLUE. (Later, we will see that it is also possible to create new colors.) For example, if you want to draw in red, you would say "g.setColor(Color.RED);". The specified color is used for all subsequent drawing operations up until the next time g.setColor() is called.
        •	g.drawLine(x1,y1,x2,y2) draws a line from the point with coordinates (x1,y1) to the point with coordinates (x2,y2).
        •	g.drawRect(x,y,w,h) draws the outline of a rectangle with vertical and horizontal sides. The parameters x, y, w, and h must be integers or integer-valued expressions. This subroutine draws the outline of the rectangle whose top-left corner is x pixels from the left edge of the drawing area and ypixels down from the top. The width of the rectangle  is w pixels, and the height is h pixels. The color that is used is black, unless a different color has been set by calling g.setColor().
        •	g.fillRect(x,y,w,h) is similar to g.drawRect() except that it fills in the inside of the rectangle instead of drawing an outline.
        •	g.drawOval(x,y,w,h) draws the outline of an oval. The oval just fits inside the rectangle that would be drawn by g.drawRect(x,y,w,h). To get a circle, use the same values for w and for h.
        •	g.fillOval(x,y,w,h) is similar to g.drawOval() except that it fills in the inside of the oval instead of drawing an outline.
        This is enough information to draw some pictures using Java graphics.
        Section 4

        EVERY SUBROUTINE IN JAVA must be defined inside some class. This makes Java rather unusual among programming languages, since most languages allow free-floating, independent subroutines. One purpose of a class is to group together related subroutines and variables. Perhaps the designers of Java felt that everything must be related to something. As a less philosophical motivation, Java's designers wanted to place firm controls on the ways things are named, since a Java program potentially has access to a huge number of subroutines created by many different programmers. The fact that those subroutines are grouped into named classes (and classes are grouped into named "packages," as we will see later) helps control the confusion that might result from so many different names.
        There is a basic distinction in Java between static and non-static subroutines. A class definition can contain the source code for both types of subroutine, but what's done with them when the program runs is very different. Static subroutines are easier to understand: In a running program, a static subroutine is a member of the class itself. Non-static subroutine definitions, on the other hand, are only there to be used when objects are created, and the subroutines themselves become members of the objects. Non-static subroutines only become relevant when you are working with objects. The distinction between static and non-static also applies to variables and to other things that can occur in class definitions. This chapter will deal with static subroutines and static variables almost exclusively. We'll turn to non-static stuff and to object-oriented programming in the next chapter.
        A subroutine that is in a class or object is often called a method, and "method" is the term that most people prefer for subroutines in Java. I will start using the term "method" occasionally; however, I will continue to prefer the more general term "subroutine" in this chapter, at least for static subroutines. However, you should start thinking of the terms "method" and "subroutine" as being essentially synonymous as far as Java is concerned.
        A subroutine must be defined somewhere. The definition has to include the name of the subroutine, enough information to make it possible to call the subroutine, and the code that will be executed each time the subroutine is called. A subroutine definition in Java takes the form:
        modifiers  return-type  subroutine-name  ( parameter-list ) {
        statements
        }
        The statements between the braces, { and }, in a subroutine definition make up the body of the subroutine. These statements are the inside, or implementation part, of the "black box," as discussed in the previous section. They are the instructions that the computer executes when the method is called. Subroutines can contain any of the statements discussed in Chapter 2 and Chapter 3.
        The modifiers that can occur at the beginning of a subroutine definition are words that set certain characteristics of the subroutine, such as whether it is static or not. The modifiers that you've seen so far are "static" and "public". There are only about a half-dozen possible modifiers altogether.
        If the subroutine is a function, whose job is to compute some value, then the return-type is used to specify the type of value that is returned by the function. It can be a type name such as String or int or even an array type such as double[]. We'll be looking at functions and return types in some detail in Section 4.4. If the subroutine is not a function, then the return-type is replaced by the special value void, which indicates that no value is returned. The term "void" is meant to indicate that the return value is empty or non-existent.
        Finally, we come to the parameter-list of the method. Parameters are part of the interface of a subroutine. They represent information that is passed into the subroutine from outside, to be used by the subroutine's internal computations. For a concrete example, imagine a class named Television that includes a method named changeChannel(). The immediate question is: What channel should it change to? A parameter can be used to answer this question. Since the channel number is an integer, the type of the parameter would be int, and the declaration of the changeChannel() method might look like
public void changeChannel(int channelNum) { ... }

        This declaration specifies that changeChannel() has a parameter named channelNum of type int. However, channelNum does not yet have any particular value. A value for channelNum is provided when the subroutine is called; for example: changeChannel(17);
        The parameter list in a subroutine can be empty, or it can consist of one or more parameter declarations of the form type parameter-name. If there are several declarations, they are separated by commas. Note that each declaration can name only one parameter. For example, if you want two parameters of type double, you have to say "double x, double y", rather than "double x, y".
        Here are a few examples of subroutine definitions, leaving out the statements that define what the subroutines do:
public static void playGame() {
        // "public" and "static" are modifiers; "void" is the
        // return-type; "playGame" is the subroutine-name;
        // the parameter-list is empty.
        . . .  // Statements that define what playGame does go here.
        }

        int getNextN(int N) {
        // There are no modifiers; "int" in the return-type;
        // "getNextN" is the subroutine-name; the parameter-list
        // includes one parameter whose name is "N" and whose
        // type is "int".
        . . .  // Statements that define what getNextN does go here.
        }

static boolean lessThan(double x, double y) {
        // "static" is a modifier; "boolean" is the
        // return-type; "lessThan" is the subroutine-name;
        // the parameter-list includes two parameters whose names are
        // "x" and "y", and the type of each of these parameters
        // is "double".
        . . .  // Statements that define what lessThan does go here.
        }

        In the second example given here, getNextN is a non-static method, since its definition does not include the modifier "static" -- and so it's not an example that we should be looking at in this chapter! The other modifier shown in the examples is "public". This modifier indicates that the method can be called from anywhere in a program, even from outside the class where the method is defined. There is another modifier, "private", which indicates that the method can be called only from inside the same class. The modifiers public and private are called access specifiers. If no access specifier is given for a method, then by default, that method can be called from anywhere in the "package" that contains the class, but not from outside that package.
        Note, by the way, that the main() routine of a program follows the usual syntax rules for a subroutine. In
public static void main(String[] args) { ... }
        the modifiers are public and static, the return type is void, the subroutine name is main, and the parameter list is "String[] args". In this case, the type for the parameter is the array type String[].
        When you define a subroutine, all you are doing is telling the computer that the subroutine exists and what it does. The subroutine doesn't actually get executed until it is called. (This is true even for the main() routine in a class -- even though you don't call it, it is called by the system when the system runs your program.) For example, the playGame() method given as an example above could be called using the following subroutine call statement:
        playGame();
        This statement could occur anywhere in the same class that includes the definition of playGame(), whether in a main() method or in some other subroutine. Since playGame() is a public method, it can also be called from other classes, but in that case, you have to tell the computer which class it comes from. Since playGame() is a static method, its full name includes the name of the class in which it is defined. Let's say, for example, that playGame() is defined in a class named Poker. Then to call playGame() from outside the Poker class, you would have to say
        Poker.playGame();
        The use of the class name here tells the computer which class to look in to find the method. It also lets you distinguish between Poker.playGame() and other potential playGame() methods defined in other classes, such as Roulette.playGame() or Blackjack.playGame().
        More generally, a subroutine call statement for a static subroutine takes the form
        subroutine-name(parameters);
        if the subroutine that is being called is in the same class, or
class-name.subroutine-name(parameters);
        if the subroutine is defined elsewhere, in a different class. (Non-static methods belong to objects rather than classes, and they are called using objects instead of class names. More on that later.) Note that the parameter list can be empty, as in the playGame() example, but the parentheses must be there even if there is nothing between them. The number of parameters that you provide when you call a subroutine must match the number listed in the parameter list in the subroutine definition, and the types of the parameters in the call statement must match the types in the subroutine definition.
        4.3.2  Formal and Actual Parameters
        Note that the term "parameter" is used to refer to two different, but related, concepts. There are parameters that are used in the definitions of subroutines, such as startingValue in the above example. And there are parameters that are used in subroutine call statements, such as the K in the statement "print3NSequence(K);". Parameters in a subroutine definition are called formal parameters or dummy parameters. The parameters that are passed to a subroutine when it is called are called actual parameters or arguments. When a subroutine is called, the actual parameters in the subroutine call statement are evaluated and the values are assigned to the formal parameters in the subroutine's definition. Then the body of the subroutine is executed.
        In order to call a subroutine legally, you need to know its name, you need to know how many formal parameters it has, and you need to know the type of each parameter. This information is called the subroutine's signature. The signature of the subroutine doTask, used as an example above, can be expressed as: doTask(int,double,boolean). Note that the signature does not include the names of the parameters; in fact, if you just want to use the subroutine, you don't even need to know what the formal parameter names are, so the names are not part of the interface.
        ava is somewhat unusual in that it allows two different subroutines in the same class to have the same name, provided that their signatures are different. When this happens, we say that the name of the subroutine is overloaded because it has several different meanings.
        Remember that the syntax of any subroutine is:
        modifiers  return-type  subroutine-name  ( parameter-list ) {
        statements
        }

        A SUBROUTINE THAT RETURNS A VALUE is called a function. A given function can only return a value of a specified type, called the return type of the function.
        4.7.1  Initialization in Declarations

        char firstInitial = 'D', secondInitial = 'E';

        int x, y = 1;   // OK, but only y has been initialized!

        int N = 3, M = N+2;  // OK, N is initialized
        //        before its value is used.

        A member variable can also be initialized at the point where it is declared, just as for a local variable. For example:
public class Bank {
    private static double interestRate = 0.05;
    private static int maxWithdrawal = 200;
     .
             .  // More variables and subroutines.
             .
}

    In Java, the modifier "final" can be applied to a variable declaration to ensure that the value stored in the variable cannot be changed after the variable has been initialized. For example, if the member variable interestRate is declared with
public final static double interestRate = 0.05;
        then it would be impossible for the value of interestRate to change anywhere else in the program.
        Chapter 5
        Programming in the Large II: Objects and Classes
        An object that is created from a class is called an instance of that class, and as the picture shows every object "knows" which class was used to create it. I've shown class PlayerData as containing something called a "constructor;" the constructor is a subroutine that creates objects.
        In Java, a class is a type, similar to the built-in types such as int and boolean. So, a class name can be used to specify the type of a variable in a declaration statement, or the type of a formal parameter, or the return type of a function. For example, a program could define a variable named std of type Student with the statement
        Student std;
        However, declaring a variable does not create an object! This is an important point, which is related to this Very Important Fact:
        In Java, no variable can ever hold an object.
        A variable can only hold a reference to an object.
        You should think of objects as floating around independently in the computer's memory. In fact, there is a special portion of memory called the heap where objects live. Instead of holding an object itself, a variable holds the information necessary to find the object in memory. This information is called a reference or pointer to the object. In effect, a reference to an object is the address of the memory location where the object is stored. When you use a variable of object type, the computer uses the reference in the variable to find the actual object.
        In a program, objects are created using an operator called new, which creates an object and returns a reference to that object. (In fact, the new operator calls a special subroutine called a "constructor" in the class.) For example, assuming that std is a variable of type Student, declared as above, the assignment statement
        std = new Student();
        would create a new object which is an instance of the class Student, and it would store a reference to that object in the variable std.
        It is possible for a variable like std, whose type is given by a class, to refer to no object at all. We say in this case that std holds a null pointer or null reference. The null pointer is written in Java as "null". You can store a null reference in the variable std by saying
        std = null;
        null is an actual value that is stored in the variable, not a pointer to something else. It is not correct to say that the variable "points to null"; in fact, the variable is null. For example, you can test whether the value of std is null by testing
        if (std == null) . . .
        Suppose that a variable that refers to an object is declared to be final. This means that the value stored in the variable can never be changed, once the variable has been initialized. The value stored in the variable is a reference to the object. So the variable will continue to refer to the same object as long as the variable exists. However, this does not prevent the data in the object from changing. The variable is final, not the object. It's perfectly legal to say
final Student stu = new Student();

        stu.name = "John Doe";  // Change data in the object;
        // The value stored in stu is not changed!
        // It still refers to the same object.
        In the opinion of many programmers, almost all member variables should be declared private. This gives you complete control over what can be done with the variable. Even if the variable itself is private, you can allow other classes to find out what its value is by providing a public accessor method that returns the value of the variable. For example, if your class contains a private member variable, title, of type String, you can provide a method
        public String getTitle() {
            return title;
        }
        that returns the value of title. By convention, the name of an accessor method for a variable is obtained by capitalizing the name of variable and adding "get" in front of the name. So, for the variable title, we get an accessor method named "get" + "Title", or getTitle(). Because of this naming convention, accessor methods are more often referred to as getter methods. A getter method provides "read access" to a variable. (Sometimes for boolean variables, "is" is used in place of "get". For example, a getter for a boolean member variable named done might be called isDone().)
        You might also want to allow "write access" to a private variable. That is, you might want to make it possible for other classes to specify a new value for the variable. This is done with a setter method. (If you don't like simple, Anglo-Saxon words, you can use the fancier term mutator method.) The name of a setter method should consist of "set" followed by a capitalized copy of the variable's name, and it should have a parameter with the same type as the variable. A setter method for the variable title could be written
public void setTitle( String newTitle ) {
        title = newTitle;
        }
        Constructors and Object Initialization
        ________________________________________
        OBJECT TYPES IN JAVA are very different from the primitive types. Simply declaring a variable whose type is given as a class does not automatically create an object of that class. Objects must be explicitly constructed. For the computer, the process of constructing an object means, first, finding some unused memory in the heap that can be used to hold the object and, second, filling in the object's instance variables. As a programmer, you don't care where in memory the object is stored, but you will usually want to exercise some control over what initial values are stored in a new object's instance variables. In many cases, you will also want to do more complicated initialization or bookkeeping every time an object is created.
        Objects are created with the operator, new. For example, a program that wants to use a PairOfDice object could say:
        PairOfDice dice;   // Declare a variable of type PairOfDice.

        dice = new PairOfDice();  // Construct a new object and store a
        //   reference to it in the variable.

        In this example, "new PairOfDice()" is an expression that allocates memory for the object, initializes the object's instance variables, and then returns a reference to the object. This reference is the value of the expression, and that value is stored by the assignment statement in the variable, dice, so that after the assignment statement is executed, dice refers to the newly created object. Part of this expression, "PairOfDice()", looks like a subroutine call, and that is no accident. It is, in fact, a call to a special type of subroutine called a constructor. This might puzzle you, since there is no such subroutine in the class definition. However, every class has at least one constructor. If the programmer doesn't write a constructor definition in a class, then the system will provide a default constructor for that class. This default constructor does nothing beyond the basics: allocate memory and initialize instance variables. If you want more than that to happen when an object is created, you can include one or more constructors in the class definition.
        The definition of a constructor looks much like the definition of any other subroutine, with three exceptions. A constructor does not have any return type (not even void). The name of the constructor must be the same as the name of the class in which it is defined. And the only modifiers that can be used on a constructor definition are the access modifiers public, private, and protected. (In particular, a constructor can't be declared static.)
        Unlike other subroutines, a constructor can only be called using the new operator, in an expression that has the form
        new class-name ( parameter-list )
        where the parameter-list is possibly empty.
        A constructor call is more complicated than an ordinary subroutine or function call. It is helpful to understand the exact steps that the computer goes through to execute a constructor call:
        1.	First, the computer gets a block of unused memory in the heap, large enough to hold an object of the specified type.
        2.	It initializes the instance variables of the object. If the declaration of an instance variable specifies an initial value, then that value is computed and stored in the instance variable. Otherwise, the default initial value is used.
        3.	The actual parameters in the constructor, if any, are evaluated, and the values are assigned to the formal parameters of the constructor.
        4.	The statements in the body of the constructor, if any, are executed.
        5.	A reference to the object is returned as the value of the constructor call.
        6.	For another example, let's rewrite the Student class that was used in Section 1. I'll add a constructor, and I'll also take the opportunity to make the instance variable, name, private.
        7.	public class Student {
8.
        9.	   private String name;                 // Student's name.
10.	   public double test1, test2, test3;   // Grades on three tests.
11.
        12.	   Student(String theName) {
        13.	        // Constructor for Student objects;
        14.	        // provides a name for the Student.
        15.	        // The name can't be null.
        16.	      if ( theName == null )
            17.	          throw new IllegalArgumentException("name can't be null");
        18.	      name = theName;
        19.	   }
20.
        21.	   public String getName() {
        22.	        // Getter method for reading the value of the private
        23.	        // instance variable, name.
        24.	      return name;
        25.	   }
26.
        27.	   public double getAverage() {
        28.	        // Compute average test grade.
        29.	      return (test1 + test2 + test3) / 3;
        30.	   }
31.
        32.	}  // end of class Student
33.	An object of type Student contains information about some particular student. The constructor in this class has a parameter of type String, which specifies the name of that student. Objects of type Student can be created with statements such as:
        If you create a class and don't explicitly make it a subclass of some other class, then it automatically becomes a subclass of the special class named Object. (Object is the one class that is not a subclass of any other class.)











































*/
