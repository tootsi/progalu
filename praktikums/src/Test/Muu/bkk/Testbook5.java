package Test.Muu.bkk;

/**
 * Created by Ats on 24.01.2017.
 */
public class Testbook5 {
}


/*
*
*A BorderLayout layout manager is designed to display one large, central component, with up to four smaller components arranged around the edges of the central component. If a container, cntr, is using a BorderLayout, then a component, comp, should be added to the container using a statement of the form

cntr.add( comp, borderLayoutPosition );
where borderLayoutPosition specifies what position the component should occupy in the layout and is given as one of the constants BorderLayout.CENTER, BorderLayout.NORTH, BorderLayout.SOUTH, BorderLayout.EAST, or BorderLayout.WEST. The meaning of the five positions is shown in this diagram:
*
*
*
*Note that a border layout can contain fewer than five components, so that not all five of the possible positions need to be filled. It would be very unusual, however, to have no center component.

A BorderLayout sets the sizes of its components as follows: The NORTH and SOUTH components (if present) are shown at their preferred heights, but their width is set equal to the full width of the container. The EAST and WEST components are shown at their preferred widths, but their height is set to the height of the container, minus the space occupied by the NORTH and SOUTH components. Finally, the CENTER component takes up any remaining space. The preferred size of the CENTER component is ignored when the layout is done, but it is taken into account when the preferred size of the container as a whole is computed. You should make sure that the components that you put into a BorderLayout are suitable for the positions that they will occupy. A horizontal slider or text field, for example, would work well in the NORTH or SOUTH position, but wouldn't make much sense in the EAST or WEST position.

The default constructor, new BorderLayout(), leaves no space between components. If you would like to leave some space, you can specify horizontal and vertical gaps in the constructor of the BorderLayout object. For example, if you say

panel.setLayout(new BorderLayout(5,7));
then the layout manager will insert horizontal gaps of 5 pixels between components and vertical gaps of 7 pixels between components. The background color of the container will show through in these gaps. The default layout for the original content pane that comes with a JFrame is a BorderLayout with no horizontal or vertical gap.

Finally, we consider the GridLayout layout manager. A grid layout lays out components in a grid containing rows and columns of equal sized rectangles. This illustration shows how the components would be arranged in a grid layout with 4 rows and 3 columns:
*
*
*
*
*If a container uses a GridLayout, the appropriate add method for the container takes a single parameter of type Component (for example: cntr.add(comp)). Components are added to the grid in the order shown; that is, each row is filled from left to right before going on the next row.

The constructor for a GridLayout takes the form "new GridLayout(R,C)", where R is the number of rows and C is the number of columns. If you want to leave horizontal gaps of H pixels between columns and vertical gaps of V pixels between rows, use "new GridLayout(R,C,H,V)" instead.

When you use a GridLayout, it's probably good form to add just enough components to fill the grid. However, this is not required. In fact, as long as you specify a non-zero value for the number of rows, then the number of columns is essentially ignored. The system will use just as many columns as are necessary to hold all the components that you add to the container. If you want to depend on this behavior, you should probably specify zero as the number of columns. You can also specify the number of rows as zero. In that case, you must give a non-zero number of columns. The system will use the specified number of columns, with just as many rows as necessary to hold the components that are added to the container.

Horizontal grids, with a single row, and vertical grids, with a single column, are very common. For example, suppose that button1, button2, and button3 are buttons and that you'd like to display them in a horizontal row in a panel. If you use a horizontal grid for the panel, then the buttons will completely fill that panel and will all be the same size. The panel can be created as follows:

JPanel buttonBar = new JPanel();
buttonBar.setLayout( new GridLayout(1,3) );
    // (Note:  The "3" here is pretty much ignored, and
    //  you could also say "new GridLayout(1,0)".
    //  To leave gaps between the buttons, you could use
    //  "new GridLayout(1,0,5,5)".)
buttonBar.add(button1);
buttonBar.add(button2);
buttonBar.add(button3);
You might find this button bar to be more attractive than the one that uses the default FlowLayout layout manager.

6.6.2  Borders

We have seen how to leave gaps between the components in a container, but what if you would like to leave a border around the outside of the container? This problem is not handled by layout managers. Instead, borders in Swing are represented by objects. A Border object can be added to any JComponent, not just to containers. Borders can be more than just empty space. The class javax.swing.BorderFactory contains a large number of static methods for creating border objects. For example, the function

BorderFactory.createLineBorder(Color.BLACK)
returns an object that represents a one-pixel wide black line around the outside of a component. If comp is a JComponent, a border can be added to comp using its setBorder() method. For example:

comp.setBorder( BorderFactory.createLineBorder(Color.BLACK) );
Once a border has been set for a JComponent, the border is drawn automatically, without any further effort on the part of the programmer. The border is drawn along the edges of the component, just inside its boundary. The layout manager of a JPanel or other container will take the space occupied by the border into account. The components that are added to the container will be displayed in the area inside the border. I don't recommend using a border on a JPanel that is being used as a drawing surface. However, if you do this, you should take the border into account. If you draw in the area occupied by the border, that part of your drawing will be covered by the border.

Here are some of the static methods that can be used to create borders:

BorderFactory.createEmptyBorder(top,left,bottom,right) -- leaves an empty border around the edges of a component. Nothing is drawn in this space, so the background color of the component will appear in the area occupied by the border. The parameters are integers that give the width of the border along the top, left, bottom, and right edges of the component. This is actually very useful when used on a JPanel that contains other components. It puts some space between the components and the edge of the panel. It can also be useful on a JLabel, which otherwise would not have any space between the text and the edge of the label.
BorderFactory.createLineBorder(color,thickness) -- draws a line around all four edges of a component. The first parameter is of type Color and specifies the color of the line. The second parameter is an integer that specifies the thickness of the border, in pixels. If the second parameter is omitted, a line of thickness 1 is drawn.
BorderFactory.createMatteBorder(top,left,bottom,right,color) -- is similar to createLineBorder, except that you can specify individual thicknesses for the top, left, bottom, and right edges of the component.
BorderFactory.createEtchedBorder() -- creates a border that looks like a groove etched around the boundary of the component. The effect is achieved using lighter and darker shades of the component's background color, and it does not work well with every background color.
BorderFactory.createLoweredBevelBorder() -- gives a component a three-dimensional effect that makes it look like it is lowered into the computer screen. As with an EtchedBorder, this only works well for certain background colors.
BorderFactory.createRaisedBevelBorder() -- similar to a LoweredBevelBorder, but the component looks like it is raised above the computer screen.
BorderFactory.createTitledBorder(title) -- creates a border with a title. The title is a String, which is displayed in the upper left corner of the border.
There are many other methods in the BorderFactory class, most of them providing variations of the basic border styles given here. The following illustration shows six components with six different border styles. The text in each component is the command that created the border for that component:
*
*
*
*(The source code for the program that produced this picture can be found in BorderDemo.java.)

6.6.3  SliderAndButtonDemo

Now that we have looked at components and layouts, it's time to put them together into some complete programs. We start with a simple demo that uses a JLabel, three JButtons, and a couple of JSliders, all laid out in a GridLayout:
*
*
*The sliders in this program control the foreground and background color of the label, and the buttons control its font style. Writing this program is a matter of creating the components, laying them out, and programming listeners to respond to events from the sliders and buttons. My program is defined as a subclass of JPanel that implements ChangeListener and ActionListener, so that the panel itself can act as the listener for change events from the sliders and action events from the buttons. In the constructor, the six components are created and configured, a GridLayout is installed as the layout manager for the panel, and the components are added to the panel:
*
*
*The class also defines the methods required by the ActionListener and ChangeListener interfaces. The actionPerformed() method is called when the user clicks one of the buttons. This method changes the font in the JLabel, where the font depends on which button was clicked. To determine which button was clicked, the method uses evt.getActionCommand(), which returns the text from the button:
*
*
*
*And the stateChanged() method, which is called when the user manipulates one of the sliders, uses the value on the slider to compute a new foreground or background color for the label. The method checks evt.getSource() to determine which slider was changed:
*
*
*
*
*As our next example, we look briefly at an example that uses nested subpanels to build a more complex user interface. The program has two JTextFields where the user can enter two numbers, four JButtons that the user can click to add, subtract, multiply, or divide the two numbers, and a JLabel that displays the result of the operation. Here is a picture from the program:
*
*
* This example uses a panel with a GridLayout that has four rows and one column. In this case, the layout is created with the statement:

setLayout(new GridLayout(4,1,3,3));
which allows a 3-pixel gap between the rows where the gray background color of the panel is visible.

The first row of the grid layout actually contains two components, a JLabel displaying the text "x =" and a JTextField. A grid layout can only have one component in each position. In this case, the component in the first row is a JPanel, a subpanel that is nested inside the main panel. This subpanel in turn contains the label and text field. This can be programmed as follows:

xInput = new JTextField("0", 10); // Create a text field sized to hold 10 chars.
JPanel xPanel = new JPanel();     // Create the subpanel.
xPanel.add( new JLabel(" x = ")); // Add a label to the subpanel.
xPanel.add(xInput);               // Add the text field to the subpanel

add(xPanel);                      // Add the subpanel to the main panel.
The subpanel uses the default FlowLayout layout manager, so the label and text field are simply placed next to each other in the subpanel at their preferred size, and are centered in the subpanel.

Similarly, the third row of the grid layout is a subpanel that contains four buttons. In this case, the subpanel uses a GridLayout with one row and four columns, so that the buttons are all the same size and completely fill the subpanel.

One other point of interest in this example is the actionPerformed() method that responds when the user clicks one of the buttons. This method must retrieve the user's numbers from the text fields, perform the appropriate arithmetic operation on them (depending on which button was clicked), and set the text of the JLabel (named answer) to represent the result. However, the contents of the text fields can only be retrieved as strings, and these strings must be converted into numbers. If the conversion fails, the label is set to display an error message:
*
*
*As mentioned above, it is possible to do without a layout manager altogether. For our next example, we'll look at a panel that does not use a layout manager. If you set the layout manager of a container to be null, by calling container.setLayout(null), then you assume complete responsibility for positioning and sizing the components in that container.

If comp is any component, then the statement

comp.setBounds(x, y, width, height);
puts the top left corner of the component at the point (x,y), measured in the coordinate system of the container that contains the component, and it sets the width and height of the component to the specified values. You should only set the bounds of a component if the container that contains it has a null layout manager. In a container that has a non-null layout manager, the layout manager is responsible for setting the bounds, and you should not interfere with its job.

Assuming that you have set the layout manager to null, you can call the setBounds() method any time you like. (You can even make a component that moves or changes size while the user is watching.) If you are writing a panel that has a known, fixed size, then you can set the bounds of each component in the panel's constructor. Note that you must also add the components to the panel, using the panel's add(component) instance method; otherwise, the component will not appear on the screen.

Our example contains four components: two buttons, a label, and a panel that displays a checkerboard pattern:
*
*
*This is just an example of using a null layout; it doesn't do anything, except that clicking the buttons changes the text of the label. (We will use this example in Section 7.5 as a starting point for a checkers game.)

The panel in this program is defined by the class NullLayoutDemo, which is created as a subclass of JPanel. The four components are created and added to the panel in the constructor. Then the setBounds() method of each component is called to set the size and position of the component:
*
*
*It's fairly easy in this case to get a reasonable layout. It's much more difficult to do your own layout if you want to allow for changes of size. In that case, you have to respond to changes in the container's size by recomputing the sizes and positions of all the components that it contains. If you want to respond to changes in a container's size, you can register an appropriate listener with the container. Any component generates an event of type ComponentEvent when its size changes (and also when it is moved, hidden, or shown). You can register a ComponentListener with the container and respond to resize events by recomputing the sizes and positions of all the components in the container. Consult a Java reference for more information about ComponentEvents. However, my real advice is that if you want to allow for changes in the container's size, try to find a layout manager to do the work for you.

The complete source code for this example is in NullLayoutDemo.java.

6.6.6  A Little Card Game

For a final example, let's look at something a little more interesting as a program. The example is a simple card game in which you look at a playing card and try to predict whether the next card will be higher or lower in value. (Aces have the lowest value in this game.) You've seen a text-oriented version of the same game in Subsection 5.4.3. Section 5.4 also introduced Deck, Hand, and Card classes that are used by the program. In this GUI version of the game, you click on a button to make your prediction. If you predict wrong, you lose. If you make three correct predictions, you win. After completing one game, you can click the "New Game" button to start a new game. Here is what the program looks like in the middle of a game:
*
*
*The complete source code for the panel can be found in the file HighLowGUI.java. I encourage you to compile and run it. Remember that you also need Card.java, Deck.java, and Hand.java, since they define classes that are used in the program.

The overall structure of the main panel in this example should be reasonably clear: It has three buttons in a subpanel at the bottom of the main panel and a large drawing surface that displays the cards and a message. (The cards and message are not components in this example; they are drawn using the graphics context in the panel's paintComponent() method.) The main panel uses a BorderLayout. The drawing surface occupies the CENTER position of the border layout. The subpanel that contains the buttons occupies the SOUTH position of the border layout, and the other three positions of the borderlayout are empty.

The drawing surface is defined by a nested class named CardPanel, which is subclass of JPanel. I have chosen to let the drawing surface object do most of the work of the game: It listens for events from the three buttons and responds by taking the appropriate actions. The main panel is defined by HighLowGUI itself, which is also a subclass of JPanel. The constructor of the HighLowGUI class creates all the other components, sets up event handling, and lays out the components:
*
*
*The programming of the drawing surface class, CardPanel, is a nice example of thinking in terms of a state machine. (See Subsection 6.4.4.) It is important to think in terms of the states that the game can be in, how the state can change, and how the response to events can depend on the state. The approach that produced the original, text-oriented game in Subsection 5.4.3 is not appropriate here. Trying to think about the game in terms of a process that goes step-by-step from beginning to end is more likely to confuse you than to help you.

The state of the game includes the cards and the message. The cards are stored in an object of type Hand. The message is a String. These values are stored in instance variables. There is also another, less obvious aspect of the state: Sometimes a game is in progress, and the user is supposed to make a prediction about the next card. Sometimes we are between games, and the user is supposed to click the "New Game" button. It's a good idea to keep track of this basic difference in state. The CardPanel class uses a boolean instance variable named gameInProgress for this purpose.

The state of the game can change whenever the user clicks on a button. The CardPanel class implements the ActionListener interface and defines an actionPerformed() method to respond to the user's clicks. This method simply calls one of three other methods, doHigher(), doLower(), or newGame(), depending on which button was pressed. It's in these three event-handling methods that the action of the game takes place.

We don't want to let the user start a new game if a game is currently in progress. That would be cheating. So, the response in the newGame() method is different depending on whether the state variable gameInProgress is true or false. If a game is in progress, the message instance variable should be set to be an error message. If a game is not in progress, then all the state variables should be set to appropriate values for the beginning of a new game. In any case, the board must be repainted so that the user can see that the state has changed. The complete newGame() method is as follows:
*
*
*The doHigher() and doLower() methods are almost identical to each other (and could probably have been combined into one method with a parameter, if I were more clever). Let's look at the doHigher() routine. This is called when the user clicks the "Higher" button. This only makes sense if a game is in progress, so the first thing doHigher() should do is check the value of the state variable gameInProgress. If the value is false, then doHigher() should just set up an error message. If a game is in progress, a new card should be added to the hand and the user's prediction should be tested. The user might win or lose at this time. If so, the value of the state variable gameInProgress must be set to false because the game is over. In any case, the board is repainted to show the new state. Here is the doHigher() method:
*
*
*The paintComponent() method of the CardPanel class uses the values in the state variables to decide what to show. It displays the string stored in the message variable. It draws each of the cards in the hand. There is one little tricky bit: If a game is in progress, it draws an extra face-down card, which is not in the hand, to represent the next card in the deck. Drawing the cards requires some care and computation. I wrote a method, "void drawCard(Graphics g, Card card, int x, int y)", which draws a card with its upper left corner at the point (x,y). The paintComponent() routine decides where to draw each card and calls this routine to do the drawing. You can check out all the details in the source code, HighLowGUI.java. (The playing cards used in this program are not very impressive. A version of the program with images that actually look like cards can be found in Subsection 13.1.3.)
*
*
*
* WE HAVE ALREADY ENCOUNTERED many of the basic aspects of GUI programming, but professional programs use many additional features. We will cover some of the advanced features of Java GUI programming in Chapter 13, but in this section we look briefly at a few more features that are essential for writing GUI programs. I will discuss these features in the context of a "MosaicDraw" program that is shown in this picture:
*
*
*The source code for the program is in the file MosaicDraw.java. The program also requires MosaicPanel.java and MosaicDrawController.java. You will want to try it out!

As the user clicks-and-drags the mouse in the large drawing area of this program, it leaves a trail of little colored squares. There is some random variation in the color of the squares. (This is meant to make the picture look a little more like a real mosaic, which is a picture made out of small colored stones in which there would be some natural color variation.) There is a menu bar above the drawing area. The "Control" menu contains commands for filling and clearing the drawing area, along with a few options that affect the appearance of the picture. The "Color" menu lets the user select the color that will be used when the user draws. The "Tools" menu affects the behavior of the mouse. Using the default "Draw" tool, the mouse leaves a trail of single squares. Using the "Draw 3x3" tool, the mouse leaves a swath of colored squares that is three squares wide. There are also "Erase" tools, which let the user set squares back to their default black color.

The drawing area of the program is a panel that belongs to the MosaicPanel class, a subclass of JPanel that is defined in MosaicPanel.java. MosaicPanel is a highly reusable class for representing mosaics of colored rectangles. It was also used behind the scenes in the sample program in Subsection 4.6.3. The MosaicPanel class does not directly support drawing on the mosaic, but it does support setting the color of each individual square. The MosaicDraw program installs a mouse listener on the panel; the mouse listener responds to mousePressed and mouseDragged events on the panel by setting the color of the square that contains the mouse. This is a nice example of applying a listener to an object to do something that was not programmed into the object itself.

The file MosaicDraw.java is a simple class that contains only the main() routine for the program. Most of the programming for MosaicDraw can be found in MosaicDrawController.java. (It might have gone into the MosaicPanel class, if I had not decided to use that pre-existing class in unmodified form.) It is the MosaicDrawController class that creates a MosaicPanel object and adds a mouse listener to it. It also creates the menu bar that is shown at the top of the program, and it implements all the commands in the menu bar. It has an instance method getMosaicPanel() that returns a reference to the mosaic panel that it has created, and it has another instance method getMenuBar() that returns a menu bar for the program. These methods are used to obtain the panel and menu bar so that they can be added to the program's window.

I urge you to study MosaicDrawController.java and MosaicDraw.java. I will not be discussing all aspects of the code here, but you should be able to understand it all after reading this section. As for MosaicPanel.java, it uses some techniques that you would not understand at this point, but I encourage you to at least read the comments in this file to learn about the API for mosaic panels.
*
*
*MosaicDraw is the first example that we have seen that uses a menu bar. Fortunately, menus are very easy to use in Java. The items in a menu are represented by the class JMenuItem (this class and other menu-related classes are in package javax.swing). Menu items are used in almost exactly the same way as buttons. In fact, JMenuItem and JButton are both subclasses of a class, AbstractButton, that defines their common behavior. In particular, a JMenuItem is created using a constructor that specifies the text of the menu item, such as:

JMenuItem fillCommand = new JMenuItem("Fill");
You can add an ActionListener to a JMenuItem by calling the menu item's addActionListener() method. The actionPerformed() method of the action listener is called when the user selects the item from the menu. You can change the text of the item by calling its setText(String) method, and you can enable it and disable it using the setEnabled(boolean) method. All this works in exactly the same way as for a JButton.

The main difference between a menu item and a button, of course, is that a menu item is meant to appear in a menu rather than in a panel. A menu in Java is represented by the class JMenu. A JMenu has a name, which is specified in the constructor, and it has an add(JMenuItem) method that can be used to add a JMenuItem to the menu. For example, the "Tools" menu in the MosaicDraw program could be created as follows, where listener is a variable of type ActionListener:
*
*
*
*Once a menu has been created, it must be added to a menu bar. A menu bar is represented by the class JMenuBar. A menu bar is just a container for menus. It does not have a name, and its constructor does not have any parameters. It has an add(JMenu) method that can be used to add menus to the menu bar. The name of the menu then appears in the menu bar. For example, the MosaicDraw program uses three menus, controlMenu, colorMenu, and toolsMenu. We could create a menu bar and add the menus to it with the statements:

JMenuBar menuBar = new JMenuBar();
menuBar.add(controlMenu);
menuBar.add(colorMenu);
menuBar.add(toolsMenu);
The final step in using menus is to use the menu bar in a window such as a JFrame. We have already seen that a frame has a "content pane." The menu bar is another component of the frame, not contained inside the content pane. The JFrame class has an instance method setMenuBar(JMenuBar) that can be used to set the menu bar. (There can only be one, so this is a "set" method rather than an "add" method.) In the MosaicDraw program, the menu bar is created by a MosaicDrawController object and can be obtained by calling that object's getMenuBar() method. The main() routine in MosaicDraw.java gets the menu bar from the controller and adds it to the window. Here is the basic code that is used (in somewhat modified form) to set up the interface:

MosaicDrawController controller = new MosaicDrawController();

MosaicPanel content = controller.getMosaicPanel();
window.setContentPane( content );  // Use panel from controller as content pane.

JMenuBar menuBar = controller.getMenuBar();
window.setJMenuBar( menuBar );    // Use the menu bar from the controller.
Using menus always follows the same general pattern: Create a menu bar. Create menus and add them to the menu bar. Create menu items and add them to the menus (and set up listening to handle action events from the menu items). Use the menu bar in a window by calling the window's setJMenuBar() method.

There are other kinds of menu items, defined by subclasses of JMenuItem, that can be added to menus. One of these is JCheckBoxMenuItem, which represents menu items that can be in one of two states, selected or not selected. A JCheckBoxMenuItem has the same functionality and is used in the same way as a JCheckBox (see Subsection 6.5.3). Three JCheckBoxMenuItems are used in the "Control" menu of the MosaicDraw program. One is used to turn the random color variation of the squares on and off. Another turns a symmetry feature on and off; when symmetry is turned on, the user's drawing is reflected horizontally and vertically to produce a symmetric pattern. And the third checkbox menu item shows and hides the "grouting" in the mosaic; the grouting is the gray lines that are drawn around each of the little squares in the mosaic. The menu item that corresponds to the "Use Randomness" option in the "Control" menu could be set up with the statements:

JMenuItem useRandomnessToggle = new JCheckBoxMenuItem("Use Randomness");
useRandomnessToggle.addActionListener(listener);  // Set up a listener.
useRandomnessToggle.setSelected(true);  // Randomness is initially turned on.
controlMenu.add(useRandomnessToggle);   // Add the menu item to the menu.
In my program, the "Use Randomness" JCheckBoxMenuItem corresponds to a boolean-valued instance variable named useRandomness in the MosaicDrawController class. This variable is part of the state of the controller object. Its value is tested whenever the user draws one of the squares, to decide whether or not to add a random variation to the color of the square. When the user selects the "Use Randomness" command from the menu, the state of the JCheckBoxMenuItem is reversed, from selected to not-selected or from not-selected to selected. The ActionListener for the menu item checks whether the menu item is selected or not, and it changes the value of useRandomness to match. Note that selecting the menu command does not have any immediate effect on the picture that is shown in the window. It just changes the state of the program so that future drawing operations on the part of the user will have a different effect. The "Use Symmetry" option in the "Control" menu works in much the same way. The "Show Grouting" option is a little different. Selecting the "Show Grouting" option does have an immediate effect: The picture is redrawn with or without the grouting, depending on the state of the menu item.

My program uses a single ActionListener to respond to all of the menu items in all the menus. This is not a particularly good design, but it is easy to implement for a small program like this one. The actionPerformed() method of the listener object uses the statement

String command = evt.getActionCommand();
to get the action command of the source of the event; this will be the text of the menu item. The listener tests the value of command to determine which menu item was selected by the user. If the menu item is a JCheckBoxMenuItem, the listener must check the state of the menu item. The menu item is the source of the event that is being processed. The listener can get its hands on the menu item object by calling evt.getSource(). Since the return value of getSource() is of type Object, the return value must be type-cast to the correct type. Here, for example, is the code that handles the "Use Randomness" command:

if (command.equals("Use Randomness")) {
        // Set the value of useRandomness depending on the menu item's state.
   JCheckBoxMenuItem toggle = (JCheckBoxMenuItem)evt.getSource();
   useRandomness = toggle.isSelected();
}
(The actionPerformed() method uses a rather long if..then..else statement to check all the possible action commands. It might be more natural and efficient use a switch statement with command as the selector and all the possible action commands as cases.)

In addition to menu items, a menu can contain lines that separate the menu items into groups. In the MosaicDraw program, the "Control" menu contains such a separator. A JMenu has an instance method addSeparator() that can be used to add a separator to the menu. For example, the separator in the "Control" menu was created with the statement:

controlMenu.addSeparator();
A menu can also contain a submenu. The name of the submenu appears as an item in the main menu. When the user moves the mouse over the submenu name, the submenu pops up. (There is no example of this in the MosaicDraw program.) It is very easy to do this in Java: You can add one JMenu to another JMenu using a statement such as mainMenu.add(submenu), and it becomes a submenu.

6.7.2  Dialogs

One of the commands in the "Color" menu of the MosaicDraw program is "Custom Color...". When the user selects this command, a new window appears where the user can select a color. This window is an example of a dialog or dialog box. A dialog is a type of window that is generally used for short, single purpose interactions with the user. For example, a dialog box can be used to display a message to the user, to ask the user a question, to let the user select a file to be opened, or to let the user select a color. In Swing, a dialog box is represented by an object belonging to the class JDialog or to a subclass.

The JDialog class is very similar to JFrame and is used in much the same way. Like a frame, a dialog box is a separate window. Unlike a frame, however, a dialog is not completely independent. Every dialog is associated with a frame (or another dialog), which is called its parent window. The dialog box is dependent on its parent. For example, if the parent is closed, the dialog box will also be closed. It is possible to create a dialog box without specifying a parent, but in that case an invisible frame is created by the system to serve as the parent.

Dialog boxes can be either modal or modeless. When a modal dialog is created, its parent frame is blocked. That is, the user will not be able to interact with the parent until the dialog box is closed. Modeless dialog boxes do not block their parents in the same way, so they seem a lot more like independent windows. In practice, modal dialog boxes are easier to use and are much more common than modeless dialogs. All the examples we will look at are modal.

Aside from having a parent, a JDialog can be created and used in the same way as a JFrame. However, I will not give any examples here of using JDialog directly. Swing has many convenient methods for creating common types of dialog boxes. For example, the color choice dialog that appears when the user selects the "Custom Color" command in the MosaicDraw program belongs to the class JColorChooser, which is a subclass of JDialog. The JColorChooser class has a static method that makes color choice dialogs very easy to use:

Color JColorChooser.showDialog(Component parentComp,
                                        String title, Color initialColor)
When you call this method, a dialog box appears that allows the user to select a color. The first parameter specifies the parent of the dialog; the parent window of the dialog will be the window (if any) that contains parentComp; this parameter can be null and it can itself be a frame or dialog object. The second parameter is a string that appears in the title bar of the dialog box. And the third parameter, initialColor, specifies the color that is selected when the color choice dialog first appears. The dialog has a sophisticated interface that allows the user to select a color. When the user presses an "OK" button, the dialog box closes and the selected color is returned as the value of the method. The user can also click a "Cancel" button or close the dialog box in some other way; in that case, null is returned as the value of the method. This is a modal dialog, and showDialog() does not return until the user dismisses the dialog box in some way. By using this predefined color chooser dialog, you can write one line of code that will let the user select an arbitrary color. Swing also has a JFileChooser class that makes it almost as easy to show a dialog box that lets the user select a file to be opened or saved.

The JOptionPane class includes a variety of methods for making simple dialog boxes that are variations on three basic types: a "message" dialog, a "confirm" dialog, and an "input" dialog. (The variations allow you to provide a title for the dialog box, to specify the icon that appears in the dialog, and to add other components to the dialog box. I will only cover the most basic forms here.)

A message dialog simply displays a message string to the user. The user (hopefully) reads the message and dismisses the dialog by clicking the "OK" button. A message dialog can be shown by calling the static method:

void JOptionPane.showMessageDialog(Component parentComp, String message)
The message can be more than one line long. Lines in the message should be separated by newline characters, \n. New lines will not be inserted automatically, even if the message is very long. For example, assuming that the special variable this refers to a Component:

JOptionPane.showMessageDialog( this, "This program is about to crash!\n"
                                      + "Sorry about that.");
An input dialog displays a question or request and lets the user type in a string as a response. You can show an input dialog by calling:

String JOptionPane.showInputDialog(Component parentComp, String question)
Again, parentComp can be null, and the question can include newline characters. The dialog box will contain an input box, an "OK" button, and a "Cancel" button. If the user clicks "Cancel", or closes the dialog box in some other way, then the return value of the method is null. If the user clicks "OK", then the return value is the string that was entered by the user. Note that the return value can be an empty string (which is not the same as a null value), if the user clicks "OK" without typing anything in the input box. If you want to use an input dialog to get a numerical value from the user, you will have to convert the return value into a number (see Subsection 3.7.2). As an example,

String name;
name = JOptionPanel.showInputDialog(null, "Hi!  What's your name?");
if (name == null)
   JOptionPane.showMessageDialog(null, "Well, I'll call you Grumpy.");
else
   JOptionPane.showMessageDialog(null, "Pleased to meet you, " + name);
Finally, a confirm dialog presents a question and three response buttons: "Yes", "No", and "Cancel". A confirm dialog can be shown by calling:

int JOptionPane.showConfirmDialog(Component parentComp, String question)
The return value tells you the user's response. It is one of the following constants:

JOptionPane.YES_OPTION -- the user clicked the "Yes" button
JOptionPane.NO_OPTION -- the user clicked the "No" button
JOptionPane.CANCEL_OPTION -- the user clicked the "Cancel" button
JOptionPane.CLOSE_OPTION -- the dialog was closed in some other way.
By the way, it is possible to omit the Cancel button from a confirm dialog by calling one of the other methods in the JOptionPane class. Just call:

JOptionPane.showConfirmDialog(
                parent, question, title, JOptionPane.YES_NO_OPTION )
The final parameter is a constant which specifies that only a "Yes" button and a "No" button should be used. The third parameter is a string that will be displayed as the title of the dialog box window.

A small demo program, SimpleDialogDemo.java is available to demonstrate JColorChooser and several JOptionPane dialogs.


*
*
*In previous sections, whenever I used a frame, I created a JFrame object in a main() routine and installed a panel as the content pane of that frame. This works fine, but a more object-oriented approach is to define a subclass of JFrame and to set up the contents of the frame in the constructor of that class. This is what I did in the case of the MosaicDraw program. MosaicDraw is defined as a subclass of JFrame. The definition of this class is very short, but it illustrates several new features of frames that I want to discuss:
*
*The constructor in this class begins with the statement super("Mosaic Draw"), which calls the constructor in the superclass, JFrame. The parameter specifies a title that will appear in the title bar of the window. The next three lines of the constructor set up the contents of the window; a MosaicDrawController is created, and the content pane and menu bar of the window are obtained from the controller. The next line is something new. If window is a variable of type JFrame (or JDialog), then the statement window.pack() will resize the window so that its size matches the preferred size of its contents. (In this case, of course, "pack()" is equivalent to "this.pack()"; that is, it refers to the window that is being created by the constructor.) The pack() method is usually the best way to set the size of a window. Note that it will only work correctly if every component in the window has a correct preferred size. This is only a problem in two cases: when a panel is used as a drawing surface and when a panel is used as a container with a null layout manager. In both these cases there is no way for the system to determine the correct preferred size automatically, and you should set a preferred size by hand. For example:

panel.setPreferredSize( new Dimension(400, 250) );
The last two lines in the constructor position the window so that it is exactly centered on the screen. The line

Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
determines the size of the screen. The size of the screen is screensize.width pixels in the horizontal direction and screensize.height pixels in the vertical direction. The setLocation() method of the frame sets the position of the upper left corner of the frame on the screen. The expression "screensize.width - getWidth()" is the amount of horizontal space left on the screen after subtracting the width of the window. This is divided by 2 so that half of the empty space will be to the left of the window, leaving the other half of the space to the right of the window. Similarly, half of the extra vertical space is above the window, and half is below.

Note that the constructor has created the window and set its size and position, but that at the end of the constructor, the window is not yet visible on the screen. (More exactly, the constructor has created the window object, but the visual representation of that object on the screen has not yet been created.) To show the window on the screen, it will be necessary to call its instance method, window.setVisible(true).

In addition to the constructor, the MosaicDraw class includes a main() routine. This makes it possible to run MosaicDraw as a stand-alone application. (The main() routine, as a static method, has nothing to do with the function of a MosaicDraw object, and it could (and perhaps should) be in a separate class.) The main() routine creates a MosaicDraw and makes it visible on the screen. It also calls

window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
which means that the program will end when the user closes the window. Note that this is not done in the constructor because doing it there would make MosaicDraw less flexible. It is possible, for example, to write a program that lets the user open multiple MosaicDraw windows. In that case, we don't want to shut down the whole program just because the user has closed one of the windows. There are other possible values for the default close operation of a window:

JFrame.DO_NOTHING_ON_CLOSE -- the user's attempts to close the window by clicking its close box will be ignored, except that it will generate a WindowEvent. A program can listen for this event and take any action it wants when the user attempts to close the window.
JFrame.HIDE_ON_CLOSE -- when the user clicks its close box, the window will be hidden just as if window.setVisible(false) were called. The window can be made visible again by calling window.setVisible(true). This is the value that is used if you do not specify another value by calling setDefaultCloseOperation.
JFrame.DISPOSE_ON_CLOSE -- the window is closed and any operating system resources used by the window are released. It is not possible to make the window visible again. (This is the proper way to permanently get rid of a window without ending the program. You can accomplish the same thing programmatically by calling the instance method window.dispose().)
6.7.4  Creating Jar Files

As the final topic for this chapter, we look again at jar files. Recall that a jar file is a "java archive" that can contain a number of class files. When creating a program that uses more than one class, it's usually a good idea to place all the classes that are required by the program into a jar file. If that is done, then a user will only need that one file to run the program. In fact, it is possible to make a so-called executable jar file. A user can run an executable jar file in much the same way as any other application, usually by double-clicking the icon of the jar file. (The user's computer must have a correct version of Java installed, and the computer must be configured correctly for this to work. The configuration is usually done automatically when Java is installed, at least on Windows and Mac OS.)

The question, then, is how to create a jar file. The answer depends on what programming environment you are using. The two basic types of programming environment -- command line and IDE -- were discussed in Section 2.6. Any IDE (Integrated Programming Environment) for Java should have a command for creating jar files. In the Eclipse IDE, for example, it can be done as follows: In the Package Explorer pane, select the programming project (or just all the individual source code files that you need). Right-click on the selection, and choose "Export" from the menu that pops up. In the window that appears, select "JAR file" and click "Next". In the window that appears next, enter a name for the jar file in the box labeled "JAR file". (Click the "Browse" button next to this box to select the file name using a file dialog box.) The name of the file should end with ".jar". If you are creating a regular jar file, not an executable one, you can hit "Finish" at this point, and the jar file will be created. To create an executable file, hit the "Next" button twice to get to the "Jar Manifest Specification" screen. At the bottom of this screen is an input box labeled "Main class". You have to enter the name of the class that contains the main() routine that will be run when the jar file is executed. If you hit the "Browse" button next to the "Main class" box, you can select the class from a list of classes that contain main() routines. Once you've selected the main class, you can click the "Finish" button to create the executable jar file. (Note that newer versions of Eclipse also have an option for exporting an executable Jar file in fewer steps.)

It is also possible to create jar files on the command line. The Java Development Kit includes a command-line program named jar that can be used to create jar files. If all your classes are in the default package (like most of the examples in this book), then the jar command is easy to use. To create a non-executable jar file on the command line, change to the directory that contains the class files that you want to include in the jar. Then give the command

jar  cf  JarFileName.jar  *.class
where JarFileName can be any name that you want to use for the jar file. The "*" in "*.class" is a wildcard that makes *.class match every class file in the current directory. This means that all the class files in the directory will be included in the jar file. If you want to include only certain class files, you can name them individually, separated by spaces. (Things get more complicated if your classes are not in the default package. In that case, the class files must be in subdirectories of the directory in which you issue the jar command. See Subsection 2.6.6.)

Making an executable jar file on the command line is more complicated. There has to be some way of specifying which class contains the main() routine. This is done by creating a manifest file. The manifest file can be a plain text file containing a single line of the form

Main-Class: ClassName
where ClassName should be replaced by the name of the class that contains the main() routine. For example, if the main() routine is in the class MosaicDraw, then the manifest file should read "Main-Class: MosaicDraw". You can give the manifest file any name you like. Put it in the same directory where you will issue the jar command, and use a command of the form

jar  cmf  ManifestFileName  JarFileName.jar  *.class
to create the jar file. (The jar command is capable of performing a variety of different operations. The first parameter to the command, such as "cf" or "cmf", tells it which operation to perform.)

By the way, if you have successfully created an executable jar file, you can run it on the command line using the command "java -jar". For example:

java  -jar  JarFileName.jar
*
*
*ARRAY BASICS HAVE BEEN DISCUSSED in previous chapters, but there are still details of Java syntax to be filled in, and there is a lot more to say about using arrays. This section looks at some of the syntactic details, with more information about array processing to come in the rest of the chapter.

To briefly review some of the basics.... An array is a numbered sequence of elements, and each element acts like a separate variable. All of the elements are of the same type, which is called the base type of the array. The array as a whole also has a type. If the base type is btype, then the array is of type btype[]. Each element in the array has an index, which is just its numerical position in the sequence of elements. If the array is A, then the i-th element of the array is A[i]. The number of elements in an array is called its length. The length of an array A is A.length. The length of an array can't be changed after the array is created. The elements of the array A are A[0], A[1], ..., A[A.length-1]. An attempt to refer to an array element with an index outside the range from zero to A.length-1 causes an ArrayIndexOutOfBoundsException.

Arrays in Java are objects, so an array variable can only refer to an array, it does not contain the array. The value of an array variable can also be null. In that case, it does not refer to any array, and an attempt to refer to an array element such as A[i] will cause a NullPointerException. Arrays are created using a special form of the new operator. For example,

int[] A = new int[10];
creates a new array with base type int and length 10, and it sets the variable A to refer to the newly created array.

7.1.1  For-each Loops

Arrays are often processed using for loops. A for loop makes it easy to process each element in an array from beginning to end. For example, if namelist is an array of Strings, then all the values in the list can be printed using

for (int i = 0; i < namelist.length; i++) {
    System.out.println( namelist[i] );
}
This type of processing is so common that there is an alternative form of the for loop that makes it easier to write. The alternative is called a for-each loop. It is probably easiest to start with an example. Here is a for-each loop for printing all the values in an array of Strings:

for ( String name : namelist ) {
    System.out.println( name );
}
The meaning of "for (String name : namelist)" is "for each string, name, in the array, namelist, do the following". The effect is that the variable name takes on each of the values in namelist in turn, and the body of the loop is executed for each of those values. Note that there is no array index in the loop. The loop control variable, name, represents one of the values in the array, not the index of one of the values.

The for-each loop is meant specifically for processing all the values in a data structure, and we will see in Chapter 10 that it applies to other data structures besides arrays. The for-each loop makes it possible to process the values without even knowing the details of how the data is structured. In the case of arrays, it lets you avoid the complications of using array indices.

A for-each loop will perform the same operation for each value that is stored in an array. If itemArray is an array of type BaseType[], then a for-each loop for itemArray has the form:

for ( BaseType item : itemArray ) {
   .
   .  // process the item
   .
}
As usual, the braces are optional if there is only one statement inside the loop. In this loop, item is the loop control variable. It is declared as a variable of type BaseType, where BaseType is the base type of the array. (In a for-each loop, the loop control variable must be declared in the loop; it cannot be a variable that already exists outside the loop.) When this loop is executed, each value from the array is assigned to item in turn and the body of the loop is executed for each value. Thus, the above loop is exactly equivalent to:

for ( int index = 0; index < itemArray.length; index++ ) {
   BaseType item;
   item = itemArray[index];  // Get one of the values from the array
     .
     .  // process the item
     .
}
For example, if A is an array of type int[], then we could print all the values from A with the for-each loop:

for ( int item : A )
   System.out.println( item );
and we could add up all the positive integers in A with:

int sum = 0;   // This will be the sum of all the positive numbers in A
for ( int item : A ) {
   if (item > 0)
      sum = sum + item;
}
The for-each loop is not always appropriate. For example, there is no simple way to use it to process the items in just a part of an array, or to process the elements in reverse order. However, it does make the code a little simpler when you do want to process all the values, in order. since it eliminates any need to use array indices.

It's important to note that a for-each loop processes the values in the array, not the elements (where an element means the actual memory location that is part of the array). For example, consider the following incorrect attempt to fill an array of integers with 17's:

int[] intList = new int[10];
for ( int item : intList ) {   // INCORRECT! DOES NOT MODIFY THE ARRAY!
   item = 17;
}
The assignment statement item = 17 assigns the value 17 to the loop control variable, item. However, this has nothing to do with the array. When the body of the loop is executed, the value from one of the elements of the array is copied into item. The statement item = 17 replaces that copied value but has no effect on the array element from which it was copied; the value in the array is not changed. The loop is equivalent to

int[] intList = new int[10];
for ( int i = 0; i < intList.length; i++ ) {
   int item = intList[i];
   item = 17;
}
which certainly does not change the value of any element in the array.

Before Java 5, every method in Java had a fixed arity. (The arity of a method is defined as the number of parameters in a call to the method.) In a fixed arity method, the number of parameters must be the same in every call to the method and must be the same as the number of formal parameters in the method's definition. Java 5 introduced variable arity methods. In a variable arity method, different calls to the method can have different numbers of parameters. For example, the formatted output method System.out.printf, which was introduced in Subsection 2.4.1, is a variable arity method. The first parameter of System.out.printf must be a String, but it can have any number of additional parameters, of any types.

Calling a variable arity method is no different from calling any other sort of method, but writing one requires some new syntax. As an example, consider a method that can compute the average of any number of values of type double. The definition of such a method could begin with:

public static double average( double...  numbers ) {
Here, the ... after the type name, double, is what makes this a variable arity method. It indicates that any number of values of type double can be provided when the subroutine is called, so that for example average(1,4,9,16), average(3.14,2.17), average(0.375), and even average() are all legal calls to this method. Note that actual parameters of type int can be passed to average. The integers will, as usual, be automatically converted to real numbers.

When the method is called, the values of all the actual parameters that correspond to the variable arity parameter are placed into an array, and it is this array that is actually passed to the method. That is, in the body of a method, a variable arity parameter of type T actually looks like an ordinary parameter of type T[]. The length of the array tells you how many actual parameters were provided in the method call. In the average example, the body of the method would see an array named numbers of type double[]. The number of actual parameters in the method call would be numbers.length, and the values of the actual parameters would be numbers[0], numbers[1], and so on. A complete definition of the method would be:

public static double average( double... numbers ) {
        // Inside this method, numbers if of type double[].
   double sum;      // The sum of all the actual parameters.
   double average;  // The average of all the actual parameters.
   sum = 0;
   for (int i = 0; i < numbers.length; i++) {
      sum = sum + numbers[i];  // Add one of the actual parameters to the sum.
   }
   average = sum / numbers.length;
   return average;
}
By the way, it is possible to pass a single array to a variable arity method, instead of a list of individual values. For example, suppose that salesData is a variable of type double[]. Then it would be legal to call average(salesData), and this would compute the average of all the numbers in the array.

The formal parameter list in the definition of a variable-arity method can include more than one parameter, but the ... can only be applied to the very last formal parameter.

As an example, consider a method that can draw a polygon through any number of points. The points are given as values of type Point, where an object of type Point has two instance variables, x and y, of type int. In this case, the method has one ordinary parameter -- the graphics context that will be used to draw the polygon -- in addition to the variable arity parameter. Remember that inside the definition of the method, the parameter points becomes an array of Points:

public static void drawPolygon(Graphics g, Point... points) {
    if (points.length > 1) {  // (Need at least 2 points to draw anything.)
       for (int i = 0; i < points.length - 1; i++) {
           // Draw a line from i-th point to (i+1)-th point
           g.drawLine( points[i].x, points[i].y, points[i+1].x, points[i+1].y );
       }
       // Now, draw a line back to the starting point.
       g.drawLine( points[points.length-1].x, points[points.length-1].y,
                       points[0].x, points[0].y );
    }
}
When this method is called, the subroutine call statement must have one actual parameter of type Graphics, which can be followed by any number of actual parameters of type Point.

For a final example, let's look at a method that strings together all of the values in a list of strings into a single, long string. This example uses a for-each loop to process the array:

public static String concat( String... values ) {
   StringBuffer buffer;  // Use a StringBuffer for more efficient concatenation.
   buffer = new StringBuffer();  // Start with an empty buffer.
   for ( String str : values ) { // A "for each" loop for processing the values.
       buffer.append(str); // Add string to the buffer.
   }
   return buffer.toString(); // return the contents of the buffer
}
Given this method definition, concat("Hello", "World") would return the string "HelloWorld", and concat() would return an empty string. Since a variable arity method can also accept an array as actual parameter, we could also call concat(lines) where lines is of type String[]. This would concatenate all the elements of the array into a single string.

We have seen that it is possible to initialize an array variable with a list of values at the time it is declared. For example,

int[] squares = { 1, 4, 9, 16, 25, 36, 49 };
This initializes squares to refer to a newly created array that contains the seven values in the list

A list initializer of this form can be used only in a declaration statement, to give an initial value to a newly declared array variable. It cannot be used in an assignment statement to assign a value to a variable that already existed. However, there is another, similar notation for creating a new array that can be used in other places. The notation uses another form of the new operator to both create a new array object and fill it with values. (The rather odd syntax is similar to the syntax for anonymous inner classes, which were discussed in Subsection 5.8.3.) As an example, to assign a new value to an array variable, cubes, of type int[], you could use:

cubes = new int[] { 1, 8, 27, 64, 125, 216, 343 };
This is an assignment statement rather than a declaration, so the array initializer syntax, without "new int[]," would not be legal here. The general syntax for this form of the new operator is

new base-type [ ] { list-of-values }
This is actually an expression whose value is a reference to a newly created array object. In this sense, it is an "array literal," since it is something that you can type in a program to represent a value. This means that it can be used in any context where an object of type base-type[] is legal. For example, you could pass the newly created array as an actual parameter to a subroutine. Consider the following utility method for creating a menu from an array of strings:


The third parameter in a call to createMenu is an array of strings. The array that is passed as an actual parameter could be created in place, using the new operator. For example, assuming that listener is of type ActionListener, we can use the following statement to create an entire File menu:

JMenu fileMenu = createMenu( "File", listener
              new String[] { "New", "Open", "Close", null, "Quit" } );
This should convince you that being able to create and use an array "in place" in this way can be very convenient, in the same way that anonymous inner classes are convenient.

By the way, it is perfectly legal to use the "new BaseType[] { ... }" syntax instead of the array initializer syntax in the declaration of an array variable. For example, instead of saying:

int[] primes = { 2, 3, 5, 7, 11, 13, 17, 19 };
you can say, equivalently,

int[] primes = new int[] { 2, 3, 5, 7, 11, 17, 19 };
In fact, rather than use a special notation that works only in the context of declaration statements, I sometimes prefer to use the second form.

One final note: For historical reasons, an array declaration such as

int[] list;
can also be written as

int list[];
which is a syntax used in the languages C and C++. However, this alternative syntax does not really make much sense in the context of Java, and it is probably best avoided. After all, the intent is to declare a variable of a certain type, and the name of that type is "int[]". It makes sense to follow the "type-name variable-name;" syntax for such declarations.


MOST EXAMPLES OF ARRAY PROCESSING that we have looked at have actually been fairly straightforward: processing the elements of the array in order from beginning to end, or random access to an arbitrary element of the array. In this section and later in the chapter, you'll see some of the more interesting things that you can do with arrays.

7.2.1  Some Processing Examples

To begin, here's an example to remind you to be careful about avoiding array indices outside the legal range. Suppose that lines is an array of type String[], and we want to know whether lines contains any duplicate elements in consecutive locations. That is, we want to know whether lines[i].equals(lines[i+1]) for any index i. Here is a failed attempt to check that condition:

boolean dupp = false;  // Assume there are no duplicates
for ( int i = 0; i < list.length; i++ ) {
    if ( lines[i].equals(lines[i+1]) ) {  // THERE IS AN ERROR HERE!
        dupp = true;   // we have found a duplicate!
        break;
    }
}
This for loop looks like many others that we have written, so what's the problem? The error occurs when i takes on its final value in the loop, i equal to lines.length-1. In that case, i+1 is equal to lines.length. But the last element in the array has index lines.length-1, so lines.length is not a legal index. This means that the reference to lines[i+1] causes an ArrayIndexOutOfBoundsException. This is easy to fix; we just need to stop the loop before i+1 goes out of range:

boolean dupp = false;  // Assume there are no duplicates
for ( int i = 0; i < list.length - 1 ; i++ ) {
    if ( lines[i].equals(lines[i+1]) ) {
        dupp = true;   // we have found a duplicate!
        break;
    }
}
This type of error can be even more insidious when working with partially full arrays (see Subsection 3.8.4), where usually only part of the array is in use, and a counter is used to keep track of how many spaces in the array are used. With a partially full array, the problem is not looking beyond the end of the array, but looking beyond the part of the array that is in use. When your program tries to look beyond the end of an array, at least the program will crash to let you know that there is a problem. With a partially full array, the problem can go undetected.

For the next example, let's continue with partially full arrays. We have seen how to add an item to a partially full array, but suppose that we also want to be able to remove items? Suppose that you write a game program, and that players can join the game and leave the game as it progresses. As a good object-oriented programmer, you probably have a class named Player to represent the individual players in the game. A list of all players who are currently in the game could be stored in an array, playerList, of type Player[]. Since the number of players can change, you will follow the partially full array pattern, and you will need a variable, playerCt, to record the number of players currently in the game. Assuming that there will never be more than 10 players in the game, you could declare the variables as:

Player[] playerList = new Player[10];  // Up to 10 players.
int      playerCt = 0;  // At the start, there are no players.
After some players have joined the game, playerCt will be greater than 0, and the player objects representing the players will be stored in the array elements playerList[0], playerList[1], ..., playerList[playerCt-1]. Note that the array element playerList[playerCt] is not in use. The procedure for adding a new player, newPlayer, to the game is simple:

playerList[playerCt] = newPlayer; // Put new player in next
                                  //     available spot.
playerCt++;  // And increment playerCt to count the new player.
But deleting a player from the game is a little harder, since you don't want to leave a "hole" in the array where the deleted player used to be. Suppose you want to delete the player at index k in playerList. The number of players goes down by one, so one fewer space is used in the array. If you are not worried about keeping the players in any particular order, then one way to do this is to move the player from the last occupied position in the array into position k and then to decrement the value of playerCt:

playerList[k] = playerList[playerCt - 1];
playerCt--;
The player previously in position k is no longer in the array, so we have deleted that player from the list. The player previously in position playerCt - 1 is now in the array twice. But it's only in the occupied or valid part of the array once, since playerCt has decreased by one. Remember that every element of the array has to hold some value, but only the values in positions 0 through playerCt - 1 will be looked at or processed in any way. (By the way, you should think about what happens if the player that is being deleted is in the last position in the list. The code does still work in this case. What exactly happens?)

Suppose that when deleting the player in position k, you'd like to keep the remaining players in the same order. (Maybe because they take turns in the order in which they are stored in the array.) To do this, all the players in positions k+1 and above must move down one position in the array. Player k+1 replaces player k, who is out of the game. Player k+2 fills the spot left open when player k+1 is moved. And so on. The code for this is

for (int i = k+1; i < playerCt; i++) {
    playerList[i-1] = playerList[i];
}
playerCt--;
Here is an illustration of the two ways of deleting an item from a partially full array. Here, player "C" is being deleted:
*
*
*This leaves open the question of what happens when a partially full array becomes full, but you still want to add more items to it? We can't change the size of the array -- but we can make a new, bigger array and copy the data from the old array into the new array. But what does it mean to copy an array in the first place?

Suppose that A and B are array variables, with the same base type, and that A already refers to an array. Suppose that we want B to refer to a copy of A. The first thing to note is that the assignment statement

B = A;
does not make a copy of A. Arrays are objects, and an array variable can only hold a pointer to an array. The assignment statement copies the pointer from A into B, and the result is that A and B now point to the same array. For example, A[0] and B[0] are just different names for exactly the same array element. To make B refer to a copy of A, we need to make an entirely new array and copy all the items from A into B. Let's say that A and B are of type double[]. Then to make a copy of A, we can say

double B;
B = new double[A.length];  // Make a new array with the same length as A.
for ( int i = 0; i < A.length; i++ ) {
    B[i] = A[i];
}
To solve the problem of adding to a partially full array that has become full, we just need to make a new array that is bigger than the existing array. The usual choice is to make a new array twice as big as the old. We need to meet one more requirement: At the end, the variable that referred to the old array must now point to the new array. That variable is what gives us access to the data, and at the end that data is in the new array. Fortunately, a simple assignment statement will make the variable point to the correct array. Let's suppose that we are using playerList and playerCt to store the players in a game, as in the example above, and we want to add newPlayer to the game. Here is how we can do that even if the playerList array is full:
*
*
*if ( playerCt == playerList.length ) {
        // The number of players is already equal to the size of the array.
        // The array is full.  Make a new array that has more space.
    Player[] temp;   // A variable to point to the new array.
    temp = new Player[ 2*playerList.length ];  // Twice as big as the old array.
    for ( int i = 0; i < playerList.length; i++ ) {
        temp[i] = playerList[i];  // Copy item from old array into new array.
    }
    playerList = temp;  // playerList now points to the new, bigger array.
}
// At this point, we know that there is room in the array for newPlayer.
playerList[playerCt] = newPlayer;
playerCt++;
After the new array has been created, there is no longer any variable that points to the old array, so it will be garbage collected.

7.2.2  Some Standard Array Methods

Copying an array seems like such a common method that you might expect Java to have a built-in method already defined to do it. In fact, Java comes with several standard array-processing methods. The methods are defined as static methods in a class named Arrays, which is in the package java.util. For example, for any array, list,

Arrays.copyOf( list, lengthOfCopy )
is a function that returns a new array whose length is given by lengthOfCopy, containing items copied from list. If lengthOfCopy is greater than list.length, then extra spaces in the new array will have their default value (zero for numeric arrays, null for object arrays, and so on). If lengthOfCopy is less than or equal to list.length, then only as many items are copied from list as will fit in the new array. So if A is any array, then

B = Arrays.copyOf( A, A.length );
sets B to refer to an exact copy of A, and

playerList = Arrays.copyOf( playerList, 2*playerList.length );
could be used to double the amount of space available in a partially full array with just one line of code. We can also use Arrays.copyOf to decrease the size of a partially full array. We might want to do that to avoid having a lot of excess, unused spaces. To implement this idea, the code for deleting player number k from the list of players might become

playerList[k] = playerList[playerCt-1];
playerCt--;
if ( playerCt < playerList.length/2 ) {
        // More than half the spaces are empty. Cut the array size in half.
    playerList = Arrays.copyOf( playerList, playerList.length/2 );
}
I should mention that class Arrays actually contains a bunch of copyOf methods, one for each of the primitive types and one for objects. I should also note that when an array of objects is copied, it is only pointers to objects that are copied into the new array. The contents of the objects are not copied. This is the usual rule for assignment of pointers.

The Arrays class contains other useful methods. I'll mention a few of them. As with Arrays.copyOf, there are actually multiple versions of all of these methods, for different array types.

Arrays.fill( array, value ) -- Fill an entire array with a specified value. The type of value must be compatible with the base type of the array. For example, assuming that numlist is an array of type double[], then Arrays.fill(numlist,17) will set every element of numlist to have the value 17.
Arrays.fill( array, fromIndex, toIndex, value ) -- Fills part of the array with value, starting at index number fromIndex and ending with index number toIndex-1. Note that toIndex itself is not included.
Arrays.toString( array ) -- A function that returns a String containing all the values from array, separated by commas and enclosed between square brackets. The values in the array are converted into strings in the same way they would be if they were printed out.
Arrays.sort( array ) -- Sorts the entire array. To sort an array means to rearrange the values in the array so that they are in increasing order. This method works for arrays of String and arrays of primitive type values (except for boolean, which would be kind of silly). But it does not work for all arrays, since it must be meaningful to compare any two values in the array, to see which is "smaller." We will discuss array-sorting algorithms in Section 7.4.
Arrays.sort( array, fromIndex, toIndex ) -- Sorts just the elements from array[fromIndex] up to array[toIndex-1]
Arrays.binarySearch( array, value ) -- Searches for value in the array. The array must already be sorted into increasing order. This is a function that returns an int. If the value is found in the array, the return value is the index of an element that contains that value. If the value does not occur in the array, the return value is -1. We will discuss the binary search algorithm in Section 7.4.
7.2.3  RandomStrings Revisited

One of the examples in Subsection 6.3.2 was a GUI program that shows multiple copies of a message in random positions, colors, and fonts. When the user clicks the program window, the positions, colors, and fonts are changed to new random values. Like several other examples from that chapter, the program had a flaw: It didn't have any way of storing the data that it would need to redraw itself. Arrays provide us with one possible solution to this problem. We can write a new version of RandomStrings that uses an array to store the position, font, and color of each string. When the panel is painted, this information is used to draw the strings, so that the panel will paint itself correctly whenever it has to be redrawn. When the user clicks, the array is filled with new random values and the panel is repainted using the new data. So, the only time that the picture will change is in response to a mouse click. The new version of the program is RandomStringsWithArray.java.

In the program, the number of copies of the message is given by a named constant, MESSAGE_COUNT. One way to store the position, color, and font of MESSAGE_COUNT strings would be to use four arrays:

int[] x = new int[MESSAGE_COUNT];
int[] y = new int[MESSAGE_COUNT];
Color[] color = new Color[MESSAGE_COUNT];
Font[] font = new Font[MESSAGE_COUNT];
These arrays would be filled with random values. In the paintComponent() method, the i-th copy of the string would be drawn at the point (x[i],y[i]). Its color would be given by color[i]. And it would be drawn in the font font[i]. This would be accomplished by the paintComponent() method

public void paintComponent(Graphics g) {
   super.paintComponent(); // (Fill with background color.)
   for (int i = 0; i < MESSAGE_COUNT; i++) {
      g.setColor( color[i] );
      g.setFont( font[i] );
      g.drawString( message, x[i], y[i] );
   }
}
This approach is said to use parallel arrays. The data for a given copy of the message is spread out across several arrays. If you think of the arrays as laid out in parallel columns -- array x in the first column, array y in the second, array color in the third, and array font in the fourth -- then the data for the i-th string can be found along the i-th row. There is nothing wrong with using parallel arrays in this simple example, but it does go against the object-oriented philosophy of keeping related data in one object. If we follow this rule, then we don't have to imagine the relationship among the data, because all the data for one copy of the message is physically in one place. So, when I wrote the program, I made a simple class to represent all the data that is needed for one copy of the message:
*
*
*To store the data for multiple copies of the message, I use an array of type StringData[]. The array is declared as an instance variable, with the name stringData:

StringData[] stringData;
Of course, the value of stringData is null until an actual array is created and assigned to it. The array has to be created and filled with data. Furthermore, each element of the array is an object of type StringData which has to be created before it can be used. The following subroutine is used to create the array and fill it with random data:

private void createStringData() {
    int width = getWidth();
    int height = getHeight();
    stringData = new StringData[MESSAGE_COUNT];
    for (int i = 0; i < MESSAGE_COUNT; i++) {
            // Create an object to represent the data for string number i,
            // and fill it with random values.
        stringData[i] = new StringData();
        int fontIndex = (int)(Math.random() * 5);
        stringData[i].font = fonts[fontIndex]; // one of 5 fonts, selected at random
        float hue = (float)Math.random();
        stringData[i].color = Color.getHSBColor(hue, 1.0F, 1.0F);  // random color
        stringData[i].x = -50 + (int)(Math.random()*(width+40));  // random x-coord
        stringData[i].y = (int)(Math.random()*(height+20));  // random y-coord
    }
}
This method is called before the panel is painted for the first time. It is also called when the user clicks the panel with the mouse, so that a mouse click will cause new random data to be created. Those are the only times when the picture can change. For example, resizing the window will cause paintComponent() to be called, but since the data hasn't changed, paintComponent() will just redraw the same picture. Here is the code from paintComponent() that draws all the strings, using the data from the array:

for (int i = 0; i < MESSAGE_COUNT; i++) {
    g.setColor( stringData[i].color );
    g.setFont( stringData[i].font );
    g.drawString( message, stringData[i].x, stringData[i].y );
}
Note that I could also have used a for-each loop here, which might be easier to understand:

for ( StringData data : stringData ) {
    g.setColor( data.color );
    g.setFont( data.font );
    g.drawString( message, data.x, data.y );
}
In this loop, the loop control variable, data, holds a copy of one of the values from the array. That value is a reference to an object of type StringData, which has instance variables named color, font, x, and y. Once again, the use of a for-each loop has eliminated the need to work with array indices.

RandomStringsWithArray uses one other array of objects. The font for a given copy of the message is chosen at random from a set of five possible fonts. In the original version, there were five variables of type Font to represent the fonts. The variables were named font1, font2, font3, font4, and font5. To select one of these fonts at random, a switch statement can be used:

Font randomFont;  // One of the 5 fonts, chosen at random.
int rand;         // A random integer in the range 0 to 4.

fontNum = (int)(Math.random() * 5);
switch (fontNum) {
   case 0:
      randomFont = font1;
      break;
   case 1:
      randomFont = font2;
      break;
   case 2:
      randomFont = font3;
      break;
   case 3:
      randomFont = font4;
      break;
   case 4:
      randomFont = font5;
      break;
}
In the new version of the program, the five fonts are stored in an array, which is named fonts. This array is declared as an instance variable of type Font[]

Font[] fonts;
The array is created in the constructor, and each element of the array is set to refer to a new Font object:

fonts = new Font[5];  // Create the array to hold the five fonts.

fonts[0] = new Font("Serif", Font.BOLD, 14);
fonts[1] = new Font("SansSerif", Font.BOLD + Font.ITALIC, 24);
fonts[2] = new Font("Monospaced", Font.PLAIN, 20);
fonts[3] = new Font("Dialog", Font.PLAIN, 30);
fonts[4] = new Font("Serif", Font.ITALIC, 36);
This makes it much easier to select one of the fonts at random. It can be done with the statements

Font randomFont;  // One of the 5 fonts, chosen at random.
int fontIndex;    // A random number in the range 0 to 4.
fontIndex = (int)(Math.random() * 5);
randomFont = fonts[ fontIndex ];
The switch statement has been replaced by a single line of code. In fact, the preceding four lines can be replaced by the single line:

Font randomFont = fonts[ (int)(Math.random() * 5) ];
This is a very typical application of arrays. Note that this example uses the random access property of arrays: We can pick an array index at random and go directly to the array element at that index.

Here is another example of the same sort of thing. Months are often stored as numbers 1, 2, 3, ..., 12. Sometimes, however, these numbers have to be translated into the names January, February, ..., December. The translation can be done very easily with an array. The array can be declared and initialized as

static String[] monthName = { "January", "February", "March",
                              "April",   "May",      "June",
                              "July",    "August",   "September",
                              "October", "November", "December" };
If mnth is a variable that holds one of the integers 1 through 12, then monthName[mnth-1] is the name of the corresponding month. We need the "-1" because months are numbered starting from 1, while array elements are numbered starting from 0. Simple array indexing does the translation for us!

7.2.4  Dynamic Arrays

Earlier, we discussed how a partially full array can be used to store a list of players in a game, allowing the list to grow and shrink over the course of the game. The list is "dynamic" in the sense that its size changes while the program is running. Dynamic lists are very common, and we might think about trying to write a class to represent the concept. By writing a class, we can avoid having to repeat the same code every time we want to use a similar data structure. We want something that is like an array, except that its size can change. Think about operations that we might want to perform on a dynamic array. Some essential and useful operations would include

add an item to the end of the array
remove the item at a specified position in the array
get the value of one of the elements in the array
set the value of one of the elements in the array
get the number of items currently in the array
When we design our class, these operations will become instance methods in that class. The items in the dynamic array will actually be stored in a normal array, using the partially full array pattern. Using what we know, the class is not difficult to write. We do have to decide what to do when an attempt is made to access an array element that doesn't exist. It seems natural to throw an index-out-of-bounds exception in that case. Let's suppose that the items in the array will be of type int.
*
*
*
*Everything here should be clear, except possibly why the original size of the items array is 8. In fact, the number 8 is arbitrary and has no effect on the functionality of the class. Any positive integer would work, but it doesn't make sense for the array to start off very big. The array will grow as needed if the number of items turns out to be large.

The example ReverseInputNumbers.java used a partially full array of int to print a list of input numbers in the reverse of the order in which they are input. In that case, an ordinary array of length 100 was used to hold the numbers. In any given run of the program, the size of the array could be much too large, or it could be too small, resulting in an exception. The program can now be written using a DynamicArrayOfInt, which will adapt itself to any number of inputs. For the program, see ReverseWithDynamicArray.java. It's a silly program, but the principle holds in any application where the amount of data cannot be predicted in advance: The size of a dynamic data structure can adapt itself to any amount of data.

This is a nice example, but there is a real problem with it. Suppose that we want to have a dynamic array of String. We can't use a DynamicArrayOfInt object to hold strings, so it looks like we need to write a whole new class, DynamicArrayOfString. If we want a dynamic array to store players in a game, we would need a class DynamicArrayOfPlayer. And so on. It looks like we have to write a dynamic array class for every possible type of data! That can't be right! In fact, Java has a solution to this problem, a standard class that implements dynamic arrays and can work with any type of data. The class is called ArrayList, and we'll see how it works in the next section.
*
*
*
*AS WE HAVE JUST SEEN in Subsection 7.2.4, we can easily encode the dynamic array pattern into a class, but it looks like we need a different class for each data type. In fact, Java has a feature called "parameterized types" that makes it possible to avoid the multitude of classes, and Java has a single class named ArrayList that implements the dynamic array pattern for all data types.

7.3.1  ArrayList and Parameterized Types

Java has a standard type ArrayList<String> that represents dynamic arrays of Strings. Similarly, there is a type ArrayList<JButton> that can be used to represent dynamic arrays of JButtons. And if Player is a class representing players in a game, then the type ArrayList<Player> can be used to represent a dynamic array of Players.

It might look like we still have a multitude of classes here, but in fact there is only one class, the ArrayList class, defined in the package java.util. But ArrayList is a parameterized type. A parameterized type can take a type parameter, so that from the single class ArrayList, we get a multitude of types including ArrayList<String>, ArrayList<JButton>, and in fact ArrayList<T> for any object type T. The type parameter T must be an object type such as a class name or an interface name. It cannot be a primitive type. This means that, unfortunately, you can not have an ArrayList of int or an ArrayList of char.

Consider the type ArrayList<String>. As a type, it can be used to declare variables, such as

ArrayList<String> namelist;
It can also be used as the type of a formal parameter in a subroutine definition, or as the return type of a subroutine. It can be used with the new operator to create objects:

namelist = new ArrayList<String>();
The object created in this way is of type ArrayList<String> and represents a dynamic list of strings. It has instance methods such as namelist.add(str) for adding a String to the list, namelist.get(i) for getting the string at index i, and namelist.size() for getting the number of items currently in the list.

But we can also use ArrayList with other types. If Player is a class representing players in a game, we can create a list of players with

ArrayList<Player> playerList = new ArrayList<Player>();
Then to add a player, plr, to the game, we just have to say playerList.add(plr). And we can remove player number k with playerList.remove(k).

When you use a type such as ArrayList<T>, the compiler will ensure that only objects of type T can be added to the list. An attempt to add an object that is not of type T will be a syntax error, and the program will not compile. However, note that objects belonging to a subclass of T can be added to the list, since objects belonging to a subclass of T are still considered to be of type T. Thus, for example, a variable of type ArrayList<JComponent> can be used to hold objects of type JButton, JPanel, JTextField, or any other subclass of JComponent. (Of course, this is the same way arrays work: An object of type T[] can hold objects belonging to any subclass of T.) Similarly, if T is an interface, then any object that implements interface T can be added to the list.

An object of type ArrayList<T> has all of the instance methods that you would expect in a dynamic array implementation. Here are some of the most useful. Suppose that list is a variable of type ArrayList<T>. Then we have:

list.size() -- This function returns the current size of the list, that is, the number of items currently in the list. The only valid positions in the list are numbers in the range 0 to list.size()-1. Note that the size can be zero. A call to the default constructor new ArrayList<T>() creates a list of size zero.
list.add(obj) -- Adds an object onto the end of the list, increasing the size by 1. The parameter, obj, can refer to an object of type T, or it can be null.
list.get(N) -- This function returns the value stored at position N in the list. The return type of this function is T. N must be an integer in the range 0 to list.size()-1. If N is outside this range, an error of type IndexOutOfBoundsException occurs. Calling this function is similar to referring to A[N] for an array, A, except that you can't use list.get(N) on the left side of an assignment statement.
list.set(N, obj) -- Assigns the object, obj, to position N in the ArrayList, replacing the item previously stored at position N. The parameter obj must be of type T. The integer N must be in the range from 0 to list.size()-1. A call to this function is equivalent to the command A[N] = obj for an array A.
list.remove(N) -- For an integer, N, this removes the N-th item in the ArrayList. N must be in the range 0 to list.size()-1. Any items in the list that come after the removed item are moved down one position. The size of the list decreases by 1.
list.remove(obj) -- If the specified object occurs somewhere in the list, it is removed from the list. Any items in the list that come after the removed item are moved down one position. The size of the ArrayList decreases by 1. If obj occurs more than once in the list, only the first copy is removed.
list.indexOf(obj) -- A function that searches for the object, obj, in the list. If the object is found in the list, then the position number where it is found is returned. If the object is not found, then -1 is returned.
For the last two methods listed here, obj is compared to an item in the list by calling obj.equals(item), unless obj is null. This means, for example, that strings are tested for equality by checking the contents of the strings, not their location in memory.

Java comes with several parameterized classes representing different data structures. Those classes make up the Java Collection Framework. Here we consider only ArrayList, but we will return to this important topic in much more detail in Chapter 10.

By the way, ArrayList can also be used as a non-parametrized type. This means that you can declare variables and create objects of type ArrayList such as

ArrayList list = new ArrayList();
The effect of this is similar to declaring list to be of type ArrayList<Object>. That is, list can hold any object that belongs to a subclass of Object. Since every class is a subclass of Object, this means that any object can be stored in list.

7.3.2  Wrapper Classes

As I have already noted, parameterized types don't work with the primitive types. There is no such thing as "ArrayList<int>". However, this limitation turns out not to be very limiting after all, because of the so-called wrapper classes such as Integer and Character.

We have already briefly encountered the classes Double and Integer in Section 2.5. These classes contain the static methods Double.parseDouble and Integer.parseInteger that are used to convert strings to numerical values, and constants such as Integer.MAX_VALUE and Double.NaN. We have also encountered the Character class in some examples, with the static method Character.isLetter, that can be used to test whether a given value of type char is a letter. There is a similar class for each of the other primitive types, Long, Short, Byte, Float, and Boolean. These classes are wrapper classes. Although they contain useful static members, they have another use as well: They are used for creating objects that represent primitive type values.

Remember that the primitive types are not classes, and values of primitive type are not objects. However, sometimes it's useful to treat a primitive value as if it were an object. This is true, for example, when you would like to store primitive type values in an ArrayList. You can't do that literally, but you can "wrap" the primitive type value in an object belonging to one of the wrapper classes.

For example, an object of type Double contains a single instance variable, of type double. The object is a "wrapper" for the double value. You can create an object that wraps the double value 6.0221415e23 with

Double d = new Double(6.0221415e23);
The value of d contains the same information as the value of type double, but it is an object. If you want to retrieve the double value that is wrapped in the object, you can call the function d.doubleValue(). Similarly, you can wrap an int in an object of type Integer, a boolean value in an object of type Boolean, and so on.

Furthermore, to make these classes even easier to use, there is automatic conversion between a primitive type and the corresponding wrapper class. For example, if you use a value of type int in a context that requires an object of type Integer, the int will automatically be wrapped in an Integer object. If you say

Integer answer = 42;
and the computer will silently read this as if it were

Integer answer = new Integer(42);
This is called autoboxing. It works in the other direction, too. For example, if d refers to an object of type Double, you can use d in a numerical expression such as 2*d. The double value inside d is automatically unboxed and multiplied by 2. Autoboxing and unboxing also apply to subroutine calls. For example, you can pass an actual parameter of type int to a subroutine that has a formal parameter of type Integer. In fact, autoboxing and unboxing make it possible in many circumstances to ignore the difference between primitive types and objects.

This is true in particular for parameterized types. Although there is no such thing as "ArrayList<int>", there is ArrayList<Integer>. An ArrayList<Integer> holds objects of type Integer, but any object of type Integer really just represents an int value in a rather thin wrapper. Suppose that we have an object of type ArrayList<Integer>:

ArrayList<Integer> integerList;
integerList = new ArrayList<Integer>();
Then we can, for example, add an object to integerList that represents the number 42:

integerList.add( new Integer(42) );
but because of autoboxing, we can actually say

integerList.add( 42 );
and the compiler will automatically wrap 42 in an object of type Integer before adding it to the list. Similarly, we can say

int num = integerList.get(3);
The value returned by integerList.get(3) is of type Integer but because of unboxing, the compiler will automatically convert the return value into an int, as if we had said

int num = integerList.get(3).intValue();
So, in effect, we can pretty much use integerList as if it were a dynamic array of int rather than a dynamic array of Integer. Of course, a similar statement holds for lists of other wrapper classes such as ArrayList<Double> and ArrayList<Character>. (There is one issue that sometimes causes problems: A list can hold null values, and a null does not correspond to any primitive type value. This means, for example, that the statement "int num = integerList.get(3);" can produce a null pointer exception in the case where integerList.get(3) returns null. Unless you are sure that all the values in your list are non-null, you need to take this possibility into account.)
*
*
*7.3.3  Programming With ArrayList

As a simple first example, we can redo ReverseWithDynamicArray.java, from the previous section, using an ArrayList instead of a custom dynamic array class. In this case, we want to store integers in the list, so we should use ArrayList<Integer>. Here is the complete program:
*
*
*currentCurve = new CurveData();        // Create a new CurveData object.

currentCurve.color = currentColor;     // The color of the curve is taken from an
                                       // instance variable that represents the
                                       // currently selected drawing color.

currentCurve.symmetric = useSymmetry;  // The "symmetric" property of the curve
                                       // is also copied from the current value
                                       // of an instance variable, useSymmetry.

currentCurve.points = new ArrayList<Point>();  // Create a new point list object.

currentCurve.points.add( new Point(evt.getX(), evt.getY()) );
           // The point where the user pressed the mouse is the first point on
           // the curve.  A new Point object is created to hold the coordinates
           // of that point and is added to the list of points for the curve.

curves.add(currentCurve);   // Add the CurveData object to the list of curves.
As the user drags the mouse, new points are added to currentCurve, and repaint() is called. When the picture is redrawn, the new point will be part of the picture.

The paintComponent() method has to use the data in curves to draw all the curves. The basic structure is a for-each loop that processes the data for each individual curve in turn. This has the form:

for ( CurveData curve : curves ) {
   .
   .  // Draw the curve represented by the object, curve, of type CurveData.
   .
}
In the body of this loop, curve.points is a variable of type ArrayList<Point> that holds the list of points on the curve. The i-th point on the curve can be obtained by calling the get() method of this list: curve.points.get(i). This returns a value of type Point which contains instance variables named x and y. We can refer directly to the x-coordinate of the i-th point as:

curve.points.get(i).x
This might seem rather complicated, but it's a nice example of a complex name that specifies a path to a desired piece of data: Go to the object, curve. Inside curve, go to points. Inside points, get the i-th item. And from that item, get the instance variable named x. Here is the complete definition of the paintComponent() method:

public void paintComponent(Graphics g) {
   super.paintComponent(g);
   Graphics2D g2 = (Graphics2D)g;
   g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                                   RenderingHints.VALUE_ANTIALIAS_ON);
   for ( CurveData curve : curves) {
      g.setColor(curve.color);
      for (int i = 1; i < curve.points.size(); i++) {
            // Draw a line segment from point number i-1 to point number i.
         int x1 = curve.points.get(i-1).x;
         int y1 = curve.points.get(i-1).y;
         int x2 = curve.points.get(i).x;
         int y2 = curve.points.get(i).y;
         g.drawLine(x1,y1,x2,y2);
         if (curve.symmetric) {
               // Also draw the horizontal and vertical reflections
               // of the line segment.
            int w = getWidth();
            int h = getHeight();
            g.drawLine(w-x1,y1,w-x2,y2);
            g.drawLine(x1,h-y1,x2,h-y2);
            g.drawLine(w-x1,h-y1,w-x2,h-y2);
         }
      }
   }
} // end paintComponent()
I encourage you to read the full source code, SimplePaint2.java, and to try out the program. In addition to serving as an example of using parameterized types, it also serves as another example of creating and using menus.



*
*
*
*
*
*
*
*
*
*7.3.4  Vectors

Early versions of Java did not include ArrayList, but they did have a very similar class named java.util.Vector that serves much the same purpose. You can still see Vectors used in older code and in many of Java's standard classes, so it's worth knowing about them. Vector is a parameterized class, so that you can use types such as Vector<String> and Vector<Integer>, but you will often see it used without the type parameter, which is essentially equivalent to using Vector<Object>.

Using a Vector is similar to using an ArrayList, except that different names are used for some commonly used instance methods, and some instance methods in one class don't correspond to any instance method in the other class. Suppose that vec is a variable of type Vector<T>. Then we have instance methods:

vec.size() -- a function that returns the number of elements currently in the vector.
vec.elementAt(N) -- returns the N-th element of the vector, for an integer N. N must be in the range 0 to vec.size()-1. This is the same as get(N) for an ArrayList.
vec.setElementAt(obj,N) -- sets the N-th element in the vector to be obj. N must be in the range 0 to vec.size()-1. This is the same as set(N,obj) for an ArrayList.
vec.addElement(obj) -- adds the Object, obj, to the end of the vector. This is the same as the add() method of an ArrayList.
vec.removeElement(obj) -- removes obj from the vector, if it occurs. Only the first occurrence is removed. This is the same as remove(obj) for an ArrayList.
vec.removeElementAt(N) -- removes the N-th element, for an integer N. N must be in the range 0 to vec.size()-1. This is the same as remove(N) for an ArrayList.
vec.setSize(N) -- sets the size of the vector to N. If there were more than N elements in vec, the extra elements are removed. If there were fewer than N elements, extra spaces are filled with null. The ArrayList class, unfortunately, does not have a corresponding setSize() method.
*
*
*TWO ARRAY PROCESSING TECHNIQUES that are particularly common are searching and sorting. Searching here refers to finding an item in the array that meets some specified criterion. Sorting refers to rearranging all the items in the array into increasing or decreasing order (where the meaning of increasing and decreasing can depend on the context). We have seen in Subsection 7.2.2 that Java has some built-in methods for searching and sorting arrays. However, a computer science student should be familiar with the algorithms that are used in those methods. In this section, you will learn some algorithms for searching and sorting.

Sorting and searching are often discussed, in a theoretical sort of way, using an array of numbers as an example. In practical situations, though, more interesting types of data are usually involved. For example, the array might be a mailing list, and each element of the array might be an object containing a name and address. Given the name of a person, you might want to look up that person's address. This is an example of searching, since you want to find the object in the array that contains the given name. It would also be useful to be able to sort the array according to various criteria. One example of sorting would be ordering the elements of the array so that the names are in alphabetical order. Another example would be to order the elements of the array according to zip code before printing a set of mailing labels. (This kind of sorting can get you a cheaper postage rate on a large mailing.)

This example can be generalized to a more abstract situation in which we have an array that contains objects, and we want to search or sort the array based on the value of one of the instance variables in the objects. We can use some terminology here that originated in work with "databases," which are just large, organized collections of data. We refer to each of the objects in the array as a record. The instance variables in an object are then called fields of the record. In the mailing list example, each record would contain a name and address. The fields of the record might be the first name, last name, street address, state, city and zip code. For the purpose of searching or sorting, one of the fields is designated to be the key field. Searching then means finding a record in the array that has a specified value in its key field. Sorting means moving the records around in the array so that the key fields of the record are in increasing (or decreasing) order.

In this section, most of my examples follow the tradition of using arrays of numbers. But I'll also give a few examples using records and keys, to remind you of the more practical applications.

7.4.1  Searching

There is an obvious algorithm for searching for a particular item in an array: Look at each item in the array in turn, and check whether that item is the one you are looking for. If so, the search is finished. If you look at every item without finding the one you want, then you can be sure that the item is not in the array. It's easy to write a subroutine to implement this algorithm. Let's say the array that you want to search is an array of ints. Here is a method that will search the array for a specified integer. If the integer is found, the method returns the index of the location in the array where it is found. If the integer is not in the array, the method returns the value -1 as a signal that the integer could not be found:
*
*
*
* This method of searching an array by looking at each item in turn is called linear search. If nothing is known about the order of the items in the array, then there is really no better alternative algorithm. But if the elements in the array are known to be in increasing or decreasing order, then a much faster search algorithm can be used. An array in which the elements are in order is said to be sorted. Of course, it takes some work to sort an array, but if the array is to be searched many times, then the work done in sorting it can really pay off.

Binary search is a method for searching for a given item in a sorted array. Although the implementation is not trivial, the basic idea is simple: If you are searching for an item in a sorted list, then it is possible to eliminate half of the items in the list by inspecting a single item. For example, suppose that you are looking for the number 42 in a sorted array of 1000 integers. Let's assume that the array is sorted into increasing order. Suppose you check item number 500 in the array, and find that the item is 93. Since 42 is less than 93, and since the elements in the array are in increasing order, we can conclude that if 42 occurs in the array at all, then it must occur somewhere before location 500. All the locations numbered 500 or above contain values that are greater than or equal to 93. These locations can be eliminated as possible locations of the number 42.

The next obvious step is to check location 250. If the number at that location is, say, -21, then you can eliminate locations before 250 and limit further search to locations between 251 and 499. The next test will limit the search to about 125 locations, and the one after that to about 62. After just 10 steps, there is only one location left. This is a whole lot better than looking through every element in the array. If there were a million items, it would still take only 20 steps for binary search to search the array! (Mathematically, the number of steps is approximately equal to the logarithm, in the base 2, of the number of items in the array.)

In order to make binary search into a Java subroutine that searches an array A for an item N, we just have to keep track of the range of locations that could possibly contain N. At each step, as we eliminate possibilities, we reduce the size of this range. The basic operation is to look at the item in the middle of the range. If this item is greater than N, then the second half of the range can be eliminated. If it is less than N, then the first half of the range can be eliminated. If the number in the middle just happens to be N exactly, then the search is finished. If the size of the range decreases to zero, then the number N does not occur in the array. Here is a subroutine that returns the location of N in a sorted array A. If N cannot be found in the array, then a value of -1 is returned instead:
*
*
*
*One particularly common application of searching is with association lists. The standard example of an association list is a dictionary. A dictionary associates definitions with words. Given a word, you can use the dictionary to look up its definition. We can think of the dictionary as being a list of pairs of the form (w,d), where w is a word and d is its definition. A general association list is a list of pairs (k,v), where k is some "key" value, and v is a value associated to that key. In general, we want to assume that no two pairs in the list have the same key. There are two basic operations on association lists: Given a key, k, find the value v associated with k, if any. And given a key, k, and a value v, add the pair (k,v) to the association list (replacing the pair, if any, that had the same key value). The two operations are usually called get and put.

Association lists are very widely used in computer science. For example, a compiler has to keep track of the location in memory associated with each variable. It can do this with an association list in which each key is a variable name and the associated value is the address of that variable in memory. Another example would be a mailing list, if we think of it as associating an address to each name on the list. As a related example, consider a phone directory that associates a phone number to each name. We'll look at a highly simplified version of this example. (This is not meant to be a realistic way to implement a phone directory.)

The items in the phone directory's association list could be objects belonging to the class:

class PhoneEntry {
   String name;
   String phoneNum;
}
The data for a phone directory consists of an array of type PhoneEntry[] and an integer variable to keep track of how many entries are actually stored in the directory. The technique of dynamic arrays (Subsection 7.2.4) can be used in order to avoid putting an arbitrary limit on the number of entries that the phone directory can hold. Using an ArrayList would be another possibility. A PhoneDirectory class should include instance methods that implement the "get" and "put" operations. Here is one possible simple definition of the class:
*
*
*
*The class defines a private instance method, find(), that uses linear search to find the position of a given name in the array of name/number pairs. The find() method is used both in the getNumber() method and in the putNumber() method. Note in particular that putNumber(name,number) has to check whether the name is in the phone directory. If so, it just changes the number in the existing entry; if not, it has to create a new phone entry and add it to the array.

This class could use a lot of improvement. For one thing, it would be nice to use binary search instead of simple linear search in the getNumber method. However, we could only do that if the list of PhoneEntries were sorted into alphabetical order according to name. In fact, it's really not all that hard to keep the list of entries in sorted order, as you'll see in the next subsection.

I will mention that association lists are also called "maps," and Java has a standard parameterized type name Map that implements association lists for keys and values of any type. You will encounter this class in Chapter 10.

7.4.3  Insertion Sort

We've seen that there are good reasons for sorting arrays. There are many algorithms available for doing so. One of the easiest to understand is the insertion sort algorithm. This technique is also applicable to the problem of keeping a list in sorted order as you add new items to the list. Let's consider that case first:

Suppose you have a sorted list and you want to add an item to that list. If you want to make sure that the modified list is still sorted, then the item must be inserted into the right location, with all the smaller items coming before it and all the bigger items after it. This will mean moving each of the bigger items up one space to make room for the new item.
*
*
*
*Conceptually, this could be extended to a sorting method if we were to take all the items out of an unsorted array, and then insert them back into the array one-by-one, keeping the list in sorted order as we do so. Each insertion can be done using the insert routine given above. In the actual algorithm, we don't really take all the items from the array; we just remember what part of the array has been sorted:
*
*
*
*Here is an illustration of one stage in insertion sort. It shows what happens during one execution of the for loop in the above method, when itemsSorted is 5:
*
*
*7.4.4  Selection Sort

Another typical sorting method uses the idea of finding the biggest item in the list and moving it to the end -- which is where it belongs if the list is to be in increasing order. Once the biggest item is in its correct location, you can then apply the same idea to the remaining items. That is, find the next-biggest item, and move it into the next-to-last space, and so forth. This algorithm is called selection sort. It's easy to write:
*
*
*static void selectionSort(int[] A) {
      // Sort A into increasing order, using selection sort

   for (int lastPlace = A.length-1; lastPlace > 0; lastPlace--) {
         // Find the largest item among A[0], A[1], ...,
         // A[lastPlace], and move it into position lastPlace
         // by swapping it with the number that is currently
         // in position lastPlace.

      int maxLoc = 0;  // Location of largest item seen so far.

      for (int j = 1; j <= lastPlace; j++) {
         if (A[j] > A[maxLoc]) {
               // Since A[j] is bigger than the maximum we've seen
               // so far, j is the new location of the maximum value
               // we've seen so far.
            maxLoc = j;
         }
      }

      int temp = A[maxLoc];  // Swap largest item with A[lastPlace].
      A[maxLoc] = A[lastPlace];
      A[lastPlace] = temp;

   }  // end of for loop

}
A variation of selection sort is used in the Hand class that was introduced in Subsection 5.4.1. (By the way, you are finally in a position to fully understand the source code for Hand class class from that section; note that it uses ArrayList. The source file is Hand.java.)

In the Hand class, a hand of playing cards is represented by an ArrayList<Card>. The objects stored in the list are of type Card. A Card object contains instance methods getSuit() and getValue() that can be used to determine the suit and value of the card. In my sorting method, I actually create a new list and move the cards one-by-one from the old list to the new list. The cards are selected from the old list in increasing order. In the end, the new list becomes the hand and the old list is discarded. This is not the most efficient procedure. But hands of cards are so small that the inefficiency is negligible. Here is the code for sorting cards by suit:
*
*
*
*This example illustrates the fact that comparing items in a list is not usually as simple as using the operator "<". In this case, we consider one card to be less than another if the suit of the first card is less than the suit of the second, and also if the suits are the same and the value of the second card is less than the value of the first. The second part of this test ensures that cards with the same suit will end up sorted by value.

Sorting a list of Strings raises a similar problem: the "<" operator is not defined for strings. However, the String class does define a compareTo method. If str1 and str2 are of type String, then

str1.compareTo(str2)
returns an int that is 0 when str1 is equal to str2, is less than 0 when str1 precedes str2, and is greater than 0 when str1 follows str2. The definition of "precedes" and "follows" for strings uses what is called lexicographic ordering, which is based on the Unicode values of the characters in the strings. Lexicographic ordering is not the same as alphabetical ordering, even for strings that consist entirely of letters (because in lexicographic ordering, all the upper case letters come before all the lower case letters). However, for words consisting strictly of the 26 lower case letters in the English alphabet, lexicographic and alphabetic ordering are the same. (The same holds true for uppercase letters.) Thus, if str1 and str2 are strings containing only letters from the English alphabet, then the test

str1.toLowerCase().compareTo(str2.toLowerCase()) < 0
is true if and only if str1 comes before str2 in alphabetical order.

Insertion sort and selection sort are suitable for sorting fairly small arrays (up to a few hundred elements, say). There are more complicated sorting algorithms that are much faster than insertion sort and selection sort for large arrays, to the same degree that binary search is faster than linear search. The standard method Arrays.sort uses these fast sorting algorithms. I'll discuss one such algorithm in Chapter 9.

7.4.5  Unsorting

I can't resist ending this section on sorting with a related problem that is much less common, but is a bit more fun. That is the problem of putting the elements of an array into a random order. The typical case of this problem is shuffling a deck of cards. A good algorithm for shuffling is similar to selection sort, except that instead of moving the biggest item to the end of the list, an item is selected at random and moved to the end of the list. Here is a subroutine to shuffle an array of ints:
*
*
*
*

Postcondition:  The items in A have been rearranged into a random order.

static void shuffle(int[] A) {
   for (int lastPlace = A.length-1; lastPlace > 0; lastPlace--) {
         // Choose a random location from among 0,1,...,lastPlace.
      int randLoc = (int)(Math.random()*(lastPlace+1));
         // Swap items in locations randLoc and lastPlace.
      int temp = A[randLoc];
      A[randLoc] = A[lastPlace];
      A[lastPlace] = temp;
   }
}
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
* */