package Test.Muu;

import lib.TextIO;

/**
 * Created by Ats on 31.12.2016.
 */
public class IfEvenIfNot {

    public static void main(String[] args) {

        System.out.println("Please insert a number: ");
        Integer N = TextIO.getInt();

        N = (N % 2 == 0) ? (N/2) : (3*N+1); //If even, divide with 2. If not, multiply with 3 and add 1

        System.out.println(N);

    }

}
