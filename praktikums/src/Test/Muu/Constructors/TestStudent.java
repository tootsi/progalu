package Test.Muu.Constructors;

public class TestStudent {

    private String name;                 // TestStudent's name.
    public double test1, test2, test3;   // Grades on three tests.

    TestStudent(String theName) {
        // Constructor for TestStudent objects;
        // provides a name for the Student.
        // The name can't be null.
        if ( theName == null )
            throw new IllegalArgumentException("name can't be null");
        name = theName;
    }

    public String getName() {
        // Getter method for reading the value of the private
        // instance variable, name.
        return name;
    }

    public double getAverage() {
        // Compute average test grade.
        return (test1 + test2 + test3) / 3;
    }

}  // end of class Student
