package Test.Muu;

import lib.TextIO;

/**
 * Created by Ats on 24.01.2017.
 */
public class ReverseStringWords2 {

    public static void main(String[] args) {

        String str;
        System.out.println("Insert a string:");
        str = TextIO.getlnString();
        String revStr = reverseWordByWord(str);
        System.out.println(reverseWordByWord(str));
    }

    public static String reverseWordByWord(String str){
        int strLeng = str.length()-1;
        String reverse = "", temp = "";

        for(int i = 0; i <= strLeng; i++){
            temp += str.charAt(i);
            if((str.charAt(i) == ' ') || (i == strLeng)){
                for(int j = temp.length()-1; j >= 0; j--){
                    reverse += temp.charAt(j);
                    if((j == 0) && (i != strLeng))
                        reverse += " ";
                }
                temp = "";
            }
        }
        return reverse;
    }
}
