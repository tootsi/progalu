package Test.Muu.ParameterUsage;

import lib.TextIO;

/**
 * This program will get a line of input from the user.  It will
 * then output one line for each character in the user's input.  That
 * line will contain 25 copies of the character.
 *
 * This simple program is meant to demonstrate the use of subroutines,
 * and especially the case where one routine calls a subroutine
 * which then calls another subroutine.  It is not a useful program.
 *
 * (Note:  It doesn't matter in what order the subroutines are
 * listed in the class.  A subroutine can always be used anywhere
 * in the class that defines it, even if the definition occurs
 * physically after the point where it is called.)
 */

public class TestCounter {

    public static void main(String[] args) {
        String input;
        System.out.println("Please insert a string: ");
        input = TextIO.getlnString();
        printRowsFromString(input);

    }

    private static void printRowsFromString (String str) {
        int j;
        for (j = 0; j < str.length(); j++) {
            printRow( str.charAt(j), 25);
        }

    }

    private static void printRow( char ch, int N ) {
        int j;
        for (j = 0; j <= N; j++) {
            System.out.println( ch );
        }
        System.out.println();
    }

}
