package Test.Muu.ParameterUsage;

/**
 * Created by Ats on 22.01.2017.
 */
public class TestDivisor {

    public static void main(String[] args) {
        printDivisor(100);
    }

    static void printDivisor (int N) {
        int D;
        for (D = 1; D < N; D++) {
            if ( N % D == 0)
                System.out.println(D);
        }
    }
}
