package Test.Muu;

/**
 * Created by Ats on 23.01.2017.
 */
public class TestIsPrime {

    /**
     * This function returns true if N is a prime number.  A prime number
     * is an integer greater than 1 that is not divisible by any positive
     * integer, except itself and 1.  If N has any divisor, D, in the range
     * 1 < D < N, then it has a divisor in the range 2 to Math.sqrt(N), namely
     * either D itself or N/D.  So we only test possible divisors from 2 to
     * Math.sqrt(N).
     */
    static boolean isPrime(int N) {

        int divisor;  // A number we will test to see whether it evenly divides N.

        if (N <= 1)
            return false;  // No number <= 1 is a prime.

        int maxToTry;  // The largest divisor that we need to test.

        maxToTry = (int)Math.sqrt(N);
        // We will try to divide N by numbers between 2 and maxToTry.
        // If N is not evenly divisible by any of these numbers, then
        // N is prime.  (Note that since Math.sqrt(N) is defined to
        // return a value of type double, the value must be typecast
        // to type int before it can be assigned to maxToTry.)

        for (divisor = 2; divisor <= maxToTry; divisor++) {
            if ( N % divisor == 0 )  // Test if divisor evenly divides N.
                return false;         // If so, we know N is not prime.
            // No need to continue testing!
        }

        // If we get to this point, N must be prime.  Otherwise,
        // the function would already have been terminated by
        // a return statement in the previous loop.

        return true;  // Yes, N is prime.

    }  // end of function isPrime

}
