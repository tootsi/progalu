package Test.Muu;

import lib.TextIO;

/**
 * Created by Ats on 23.01.2017.
 */
public class TestReverseString {

    static String reverse(String str) {
        String copy;  // The reversed copy.
        int i;        // One of the positions in str,
        //       from str.length() - 1 down to 0.
        copy = "";    // Start with an empty string.
        for ( i = str.length() - 1;  i >= 0;  i-- ) {
            // Append i-th char of str to copy.
            copy = copy + str.charAt(i);
        }
        return copy;
    }
}
