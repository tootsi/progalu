package Test.Loops;

/**
 * Created by Ats on 12.11.2016.
 */
public class Test5 {
    public static void main(String[] args) {
        int x = 7;
        int y = 6;

        int sum = add(x, y);

        System.out.println(sum);

        writeXTimes(5, "I will do something!");
        //writeXTimes(sum, "I will do something!");
    }


    public static int add (int a, int b) {
        int sum = a + b;

        return sum;
    }

    public static void writeXTimes( int howManyTimes, String whatYouAreWriting) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(whatYouAreWriting);
        }
    }
}
