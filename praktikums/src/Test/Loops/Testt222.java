package Test.Loops;

/**
 * Created by Ats on 16.11.2016.
 */
public class Testt222 {
    public static void main(String[] args) {
    int[][] res = muster(9);

    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            System.out.print(res[i][j]);
        } // sisemine
    } // v6limine
} // main

    public static int[][] muster(int n) {
        int[][] ruudud = new int[n][n];

        for (int x = 0; x < n; x++) {
            for (int y = 0; y < n; y++) {
                ruudud[x][y] = (int) Math.pow((y + x), 2);
            } // v6limine
        } // sisemine
        return ruudud;
    }
}