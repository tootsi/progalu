package Test.Loops;

/**
 * Created by Ats on 3.01.2017.
 */
public class PrintAlphabet {

    public static void main(String[] args) {

        char ch;

        for (ch = 'A'; ch <= 'Z' ; ch++) {
            System.out.print(ch);

        }

    }

}
