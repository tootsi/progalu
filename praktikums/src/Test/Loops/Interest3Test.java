package Test.Loops;

import lib.TextIO;

/**
 * Created by Ats on 1.01.2017.
 */
public class Interest3Test {

    public static void main(String[] args) {

        Double sum;
        Double interest;
        Integer years;

        System.out.println("Enter the invested sum:");
        sum = TextIO.getDouble();
        System.out.println("Enter the interest percentage");
        interest = TextIO.getDouble();

        years = 0;

        while (years < 5) {
            sum = sum + sum * interest;
            System.out.println(sum);
            years = years + 1;
        }


    }

}
