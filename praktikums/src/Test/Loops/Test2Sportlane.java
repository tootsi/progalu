package Test.Loops;

/**
 * Created by Ats on 16.11.2016.
 */
public class Test2Sportlane {

    public static void main (String[] args) {
        System.out.println (score (new int[]{4, 1, 2, 3, 0})); // 9
        // Your tests here
    }

    public static int score (int[] points) {
        int smallest = Integer.MAX_VALUE;
        int secondSmallest = Integer.MAX_VALUE;
        int sum = 0;
        for (int i = 0; i < points.length; i++) {
            if (points[i] < smallest) {
                secondSmallest = smallest;
                smallest = points[i];
            } else if (points[i] < secondSmallest) {
                secondSmallest = points[i];
            }
            sum += secondSmallest;
        }

        return sum;

        //return 0; // TODO!!! Your program here
    }

}
