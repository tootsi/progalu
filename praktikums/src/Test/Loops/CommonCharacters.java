package Test.Loops;

import lib.TextIO;

/**
 * Created by Ats on 3.01.2017.
 */
public class CommonCharacters {

    public static void main(String[] args) {

        String s1;
        String s2;

        System.out.println("Enter first string: ");
        s1 = TextIO.getlnString();
        System.out.println("Enter second string: ");
        s2 = TextIO.getlnString();

        boolean nothingIsCommon;
        nothingIsCommon = true; // Assume s1 and s2 have no chars in common.
        int i,j; // Variables for iterating through the chars in s1 and s2.

        i = 0;
        bigloop: while (i < s1.length()) {
            j = 0;
            while (j < s2.length()) {
                if (s1.charAt(i) == s2.charAt(j)) { // s1 and s2 have a common char.
                    nothingIsCommon = false;
                    break bigloop;
                }
                j++;  // Go on to the next char in s2.
            }
            i++;  //Go on to the next char in s1.

        }


    }

}
