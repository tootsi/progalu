package Test.Loops;

import lib.TextIO;

/**
 * Created by Ats on 3.01.2017.
 */
public class ListLetter2 {

    public static void main(String[] args) {

        String str;
        int count;
        char letter;


        System.out.println("Please type in a line of text.");
        str = TextIO.getln();

        str = str.toUpperCase();

        count = 0;
        for ( letter = 'A'; letter < 'Z' ; letter++) {

            if (str.indexOf(letter) >= 0)
                System.out.println(letter);

        }

    }

}
