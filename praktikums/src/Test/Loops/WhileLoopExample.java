package Test.Loops;

/**
 * Created by Ats on 1.01.2017.
 */
public class WhileLoopExample {

    public static void main(String[] args) {

        int number;
        number = 1;
        while ( number < 600) {
            System.out.println(number);
            number = number + 1;
        }

        System.out.println("Done!");

    }

}
