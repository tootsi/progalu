package Test.Revision;

import lib.TextIO;

/*
Write a program that reverses the order of words (only counting space as a separator) in a String.
*/

public class Ex1 {

    public static void main(String[] args) {

        String InsText;
        System.out.println("Please insert strings: ");
        InsText = TextIO.getlnString();
        //reverse(InsText);
        System.out.print(reverse(InsText));

    }

    public static String reverse(String tekst) {

        StringBuilder sb = new StringBuilder(tekst.length() + 1);
        String[] words = tekst.split(" ");
        for (int i = words.length - 1; i >= 0; i--) {
            sb.append(words[i]).append(' ');
        }
        sb.setLength(sb.length() - 1);  // Strip trailing space
        return sb.toString();

    }

}
