package Test.Revision.MyExperiments;

import java.util.ArrayList;

public class MorePrep {
    public static void main(String[] args) {

// (1) Write a method that would return from a given array all numbers that are not increasing

// (2) Write a program that would return an array of words from the given array of words,
// so that all words containing two same kind of characters in a row are removed.

// (3) Write a program that reverses the order of words (only counting space as a separator) in a String

// (4) write a program that finds the arithmetic average of characters from a given array of words

// (5) Write a method in java to check whether a given integer n is negative odd number.

// (6) write a method that reverses the order of elements in the given array.

// (7) write a method that creates a String from given two-dimensional array of characters.
// Every word is in its own column, space characters found in the array must not be included.
// Words must be separated by spaces. The rows of the two-dimensional character array might differ in lengths.


        //(1)
        int[] x = {1, 2, 5, 7, 4, 8, 3, 2, 9};
        int[] y = increaseNumbers(x);
        for (int z : y) {
            System.out.printf("%-3d", z);
        }

        System.out.println();

        //(2)
        String[] a = {"One", "Two", "Three", "Four", "Five", "Ssix"};
        for (String b : removeDuplicates(a)) {
            System.out.print(b + " ");
        }

        System.out.println();

        //(3)
        String test = "Hello this is a test String";
        System.out.println(reverseOrder(test));

        //(4)
        String[] average = {"One", "Two", "Three", "Four", "Five", "Six"};
        System.out.println(giveAverageWordLength(average));

        //(5)
        int n = -13;
        System.out.println(negativeOdd(n));

        //(6)
        int[] exampleArray = {1, 2, 3, 6, 7, 8};
        for (int c : reverseOrderV2(exampleArray)) {
            System.out.printf("%-3d", c);
        }

        System.out.println();

        //(7)
        char[][] doubleTrouble = {{'t', 'h', 'e'}, {'c', 'a', 'r'}, {'i', 's', 't'}, {'k', 'a', 'p', 'u', 't', 't'}};
        System.out.print(giveMeAString(doubleTrouble));

    }

    public static int[] increaseNumbers(int[] x) {
        ArrayList<Integer> result = new ArrayList<>();
        int lastAdded = Integer.MIN_VALUE;
        for (int i = 0; i < x.length; i++) {
            if (x[i] > lastAdded) {
                result.add(x[i]);
                lastAdded = x[i];
            }
        }

        int[] realResult = new int[result.size()];
        for (int i = 0; i < realResult.length; i++) {
            realResult[i] = result.get(i);
        }
        return realResult;
    }

    public static String[] removeDuplicates(String[] array) {
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            int counter = 0;
            for (int j = 1; j < array[i].length(); j++) {
                if (array[i].toLowerCase().charAt(j) == array[i].toLowerCase().charAt(j - 1)) {
                } else {
                    counter++;
                }
            }
            if (counter == array[i].length() - 1) {
                result.add(array[i]);
            }
        }
        String[] realResult = new String[result.size()];
        for (int i = 0; i < realResult.length; i++) {
            realResult[i] = result.get(i);
        }
        return realResult;
    }

    public static String reverseOrder(String test) {
        String[] result = test.split(" ");
        String realResult = "";
        for (int i = result.length - 1; i >= 0; i--) {
            if (i == 0) {
                realResult += result[i];
            } else {
                realResult += result[i] + " ";
            }
        }
        return realResult;
    }

    public static int giveAverageWordLength(String[] input) {
        int sum = 0;
        for (int i = 0; i < input.length; i++) {
            sum += input[i].length();
        }
        return sum / input.length;
    }

    public static boolean negativeOdd(int input) {
        if (input % 2 != 0 && input < 0) {
            return true;
        } else {
            return false;
        }
    }

    public static int[] reverseOrderV2(int[] input) {
        int[] result = new int[input.length];
        int counter = 0;
        for (int i = input.length - 1; i >= 0; i--) {
            result[counter] = input[i];
            counter++;
        }
        return result;
    }

    public static String giveMeAString(char[][] input) {
        String result = "";
        for (int i = 0; i < input.length; i++) {
            String word = "";
            for (int j = 0; j < input[i].length; j++) {
                if (input[i][j]==' '){
                    continue;
                }else {
                    word += input[i][j];
                }
            }
            if (i==input.length-1){
                result += word;
            }else {
                result += word + " ";
            }
        }
        return result;
    }

}
