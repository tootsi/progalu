package Test.Revision.MyExperiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program that would return an array of words from the given array of words, so that all words containing
 * two same kind of characters in a row are removed.
 */

public class Ex2_2 {

    public static void main(String[] args) {

        String[] words = {"one", "two", "three", "four", "mikk"};

        List<String> list = new ArrayList<String>(Arrays.asList(words));
        list.remove("three");
        list.remove("mikk");
        words = list.toArray(new String[0]);
        System.out.println(Arrays.toString(words));

    }
}