package Test.BranchingStatements;

/**
 * Created by Ats on 3.01.2017.
 */
public class EvenNrBetw20 {

    public static void main(String[] args) {

        for (int N = 0; N < 20; N = N + 2) {
            System.out.println(N);
        }

    }

}

//Alternatives:
/*
       for (N = 1; N <= 10; N++) {
               System.out.println( 2*N );
               }*/

/*       for (N = 2; N <= 20; N++) {
               if ( N % 2 == 0 ) // is N even?
               System.out.println( N );
               } */

/*       for (N = 1; N <= 1; N++) {
               System.out.println("2 4 6 8 10 12 14 16 18 20");
               }*/
