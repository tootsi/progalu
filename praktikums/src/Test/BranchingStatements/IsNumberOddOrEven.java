package Test.BranchingStatements;

import lib.TextIO;

/**
 * Created by Ats on 31.12.2016.
 */
public class IsNumberOddOrEven {

    public static void main(String[] args) {

        System.out.println("Please insert a number:");
        int x = TextIO.getlnInt();
        if (x % 2 == 0) {
            System.out.println("This is an even number");
        } else {
            System.out.println("This is an odd number");
        } return;

    }

}
