package Test.BranchingStatements;

import lib.TextIO;

/**
 * Created by Ats on 31.12.2016.
 */
public class MakeNumberEven {

    public static void main(String[] args) {

        System.out.println("Please insert an integer: ");
        int x = TextIO.getlnInt();
        if (x % 2 == 0) {
            System.out.println("Number is already even.");
        } else {
            System.out.println("New number is: ");
            System.out.println(x + 1);
        }


    }

}
