package Test.BranchingStatements;

import lib.TextIO;

/**
 * Created by Ats on 3.01.2017.
 */
public class PrintInsIntsInIncreasingOrder {

    public static void main(String[] args) {

        int x;
        int y;
        int z;

        System.out.println("Please insert first number: ");
        x = TextIO.getlnInt();
        System.out.println("Please insert second number: ");
        y = TextIO.getlnInt();
        System.out.println("Please insert thrid number: ");
        z = TextIO.getlnInt();

        if (x < y && x < z) {        // x comes first
            if (y < z)
                System.out.println( x + " " + y + " " + z );
            else
                System.out.println( x + " " + z + " " + y );
        }
        else if (x > y && x > z) {   // x comes last
            if (y < z)
                System.out.println( y + " " + z + " " + x );
            else
                System.out.println( z + " " + y + " " + x );
        }
        else {                       // x in the middle
            if (y < z)
                System.out.println( y + " " + x + " " + z);
            else
                System.out.println( z + " " + x + " " + y);
        }

//Alternative way:
/*
        if ( x < y ) {  // x comes before y
            if ( z < x )   // z comes first
                System.out.println( z + " " + x + " " + y);
            else if ( z > y )   // z comes last
                System.out.println( x + " " + y + " " + z);
            else   // z is in the middle
                System.out.println( x + " " + z + " " + y);
        }
        else {          // y comes before x
            if ( z < y )   // z comes first
                System.out.println( z + " " + y + " " + x);
            else if ( z > x )  // z comes last
                System.out.println( y + " " + x + " " + z);
            else  // z is in the middle
                System.out.println( y + " " + z + " " + x);
        }*/



//My half-assed attempt:
/*        if (x > y && y > z) {
            System.out.println(x);
            System.out.println(y);
            System.out.println(z);
        } else if (y > x && x > z) {
            System.out.println(y);
            System.out.println(x);
            System.out.println(z);
        } else if (z > x && )*/

    }


}
