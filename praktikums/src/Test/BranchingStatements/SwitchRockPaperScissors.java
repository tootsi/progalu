package Test.BranchingStatements;

/**
 * Created by Ats on 4.01.2017.
 */
public class SwitchRockPaperScissors {

    public static void main(String[] args) {

        String computerMove;

        switch ( (int)(3*Math.random()) ) {
            case 0:
                computerMove = "Rock";
                break;
            case 1:
                computerMove = "Paper";
                break;
            default:
                computerMove = "Scissors";
                break;
        }

        System.out.println(computerMove);

    }

}
