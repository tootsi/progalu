package praktikum3;

import lib.TextIO;

/**
 * Created by Ats on 19.09.2016.
 */
public class CumLaude_IMPROVED {
    public static void main(String[] args) {

        System.out.println("Please insert your average grade:");
        double agrade = TextIO.getlnDouble();

        if (agrade < 0) {
            System.out.print("Number entered is in wrong format!");
        }

        System.out.println("Please insert your thesis grade");
        int tgrade = TextIO.getlnInt();

        if (agrade >= 4.5 && tgrade >= 5) {
            System.out.print("You will be given Cum Laude!");
        }   else{
            System.out.print("You will not be given Cum Laude!");
        }
    }
}
