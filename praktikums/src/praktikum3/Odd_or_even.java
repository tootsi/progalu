package praktikum3;

import lib.TextIO;

/**
 * Created by Ats on 18.09.2016.
 */
public class Odd_or_even {
    public static void main(String[] args) {

        System.out.println("Please insert a number?");
        int input = TextIO.getlnInt();

        if (input % 2 == 0) {
            System.out.println("Number is Even.");
        } else{
            System.out.println("Number is odd.");
        }

    }

}
