package praktikum3;

import lib.TextIO;

/**
 * Created by Ats on 19.09.2016.
 */
public class Couple_IMPROVED {
    public static void main(String[] args) {

        System.out.println("Please insert 1st person's age:");
        int cp1 = TextIO.getlnInt();

        System.out.println("Please insert 2nd person's age");
        int cp2 = TextIO.getlnInt();

        int agediff;
        if (cp1 > cp2) {
            agediff = cp1 - cp2;
        } else {
            agediff = cp2 - cp1;
        }

        if (agediff < 5) {
            System.out.println("Very nice!");
        } else if (agediff >= 5 & agediff <= 10) {
            System.out.println("Quite okay.");
        } else if (agediff >= 11 & agediff <= 15) {
            System.out.println("Not that okay.");
        } else {
            System.out.println("Not okay.");
        }
    }
}
