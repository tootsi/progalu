package praktikum3;

import lib.TextIO;

/**
 * Created by Ats on 18.09.2016.
 */
public class CumLaude {
    public static void main(String[] args) {

        System.out.println("Please insert your average grade:");
        double agrade = TextIO.getlnDouble();

        System.out.println("Please insert your thesis grade");
        int tgrade = TextIO.getlnInt();

        if (agrade >= 4.5 && tgrade >= 5) {
            System.out.print("You will be given Cum Laude!");
        }   else{
            System.out.print("You will not be given Cum Laude!");
        }
    }
}
