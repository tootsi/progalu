package praktikum3;

import lib.TextIO;

import java.util.Objects;

/**
 * Created by Ats on 18.09.2016.
 */
public class Password {
    public static void main(String[] args) {

        System.out.println("Please insert password:");
        String input = TextIO.getlnString();
        String password = "P@ssword1";

        //Objects.equals("P@ssword1", new )

        if (password.equals(input)) {
            System.out.print("Password is correct!");
        } else {
            System.out.print("Password is incorrect!");

            //password.equals(input);
        }
    }
}
