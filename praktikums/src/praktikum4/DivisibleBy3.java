package praktikum4;

/**
 * Created by Ats on 22.10.2016.
 */
public class DivisibleBy3 {
    public static void main(String[] args) {
        for (int i = 30; i > 0; i -= 3)
            System.out.println(i + " ");
    }
}
