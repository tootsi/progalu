package praktikum4;

/**
 * Created by Ats on 12.11.2016.
 */
public class Dice {
    public static void main (String [] args) {
        String dice[][] = {
                {
                        "+-------+",
                        "|       |",
                        "|   *   |",
                        "|       |",
                        "+-------+"
                },
                {
                        "+-------+",
                        "|       |",
                        "| *   * |",
                        "|       |",
                        "+-------+"
                },
                {
                        "+-------+",
                        "| *     |",
                        "|   *   |",
                        "|     * |",
                        "+-------+"
                },
                {
                        "+-------+",
                        "| *   * |",
                        "|       |",
                        "| *   * |",
                        "+-------+"
                },
                {
                        "+-------+",
                        "| *   * |",
                        "|   *   |",
                        "| *   * |",
                        "+-------+"
                },
                {
                        "+-------+",
                        "| *   * |",
                        "| *   * |",
                        "| *   * |",
                        "+-------+"
                }
        };
        int t1 = (int)(Math.random() * dice.length);

        for (int i = 0; i < dice[t1].length; i++) {
            System.out.println(dice[t1][i]);
        }

    }
}