package praktikum4;

import lib.TextIO;

/**
 * Created by Ats on 22.10.2016.
 */
public class Table2Alt {
    public static void main(String[] args) {

        System.out.println("Please enter the number of rows:");
        int size = TextIO.getInt();

        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                boolean firstrow = row == 0;
                boolean lastrow = row == size - 1;
                if (firstrow || lastrow) {
                    System.out.println("--");
                } else {
                    boolean firstColumn = col == 0;
                    boolean lastColumn = col == size - 1;
                    if (firstColumn || lastColumn) {
                        System.out.println("| ");
                    } else {
                        boolean mainDiegonal = row == col;
                        boolean sideDiegonal = col + row == size - 1;
                        if (mainDiegonal || sideDiegonal) {
                            System.out.println("x ");
                        } else {
                            System.out.println("0 ");
                        }
                    }


/*                {
                    System.out.println("--");
                } else if (col == 0 || col == size - 1) {
                    System.out.println("| ");
                } else if (row == col || col + row == size -1 ) {
                    System.out.println("1 ");
                } else {
                    System.out.println("0 ");*/
                }
            }
        }
    }
}
