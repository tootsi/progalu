package praktikum4;

import lib.TextIO;

/**
 * Created by Ats on 22.10.2016.
 */
public class Table1 {
    public static void main(String[] args) {

        System.out.println("Please enter the number of rows:");
        int rows = TextIO.getInt();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                if (i == j) {
                    System.out.println("x ");
                } else {
                    System.out.println("0 ");
                }
            }
            System.out.println();
        }
    }
}
