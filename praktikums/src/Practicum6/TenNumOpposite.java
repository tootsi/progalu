package Practicum6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import lib.TextIO;

public class TenNumOpposite {
    public static void main(String[] args) {
//Write a program that lets user insert 10 numbers and then prints those numbers in opposite order.

/*        reverse10Numbers();*/
    reverse10NumbersArrayMethod();

        // System.out.println(args[0]);      // Excercise 3 from practcum 6



    }

    public static void reverse10Numbers() {

        int[] numbers = new int[10];
        for (int i = 0; i < 10; i++) {
            System.out.println("Enter number: ");
            numbers[i] = TextIO.getInt();

        }
//      Reversing numbers method:
        for (int i = numbers.length - 1; i >= 0 ; i--) {
            System.out.println(numbers[i]);

        }

        /*int[] reversed = reverseNumbers(numbers);

        for (int i = 0; i < reversed.length; i++) {
            System.out.println(reversed[i]);

        }*/

    }

    public static void reverse10NumbersArrayMethod() {

        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            System.out.println("Enter number: ");
            numbers.add(TextIO.getInt());

        }

        Collections.reverse(numbers);
        System.out.println(numbers);
    }

    public static int[] reverseNumbers(int[] arrayToReverse) {

        int[] reversedArray = new int[arrayToReverse.length]; // The size of the reversed array needs to be the same size as the original Array
        for (int i = 0; i < arrayToReverse.length; i++) {
            reversedArray[arrayToReverse.length - 1 - i] = arrayToReverse[i]; //Take the value from the original array [arratToReverse[i]) -> Assign it to the reverse array

        }

        return reversedArray;
    }

/*    public static void tenNumbersArray() {

        ArrayList<> names = new ArrayList<>();*/

    /*    for (int i = 0; i < tenNumbersArray(); i++) {
            System.out.println("Please enter the name number: " + (i+1));
            names.add(TextIO.getWord());*/

    //}

/*
        public static void randomNameWithArrayList() {
        System.out.println("How many names: ");
        int numberOfNames = TextIO.getInt();

        ArrayList<String> names = new ArrayList<>();

        for (int i = 0; i < numberOfNames; i++) {
            System.out.println("Please enter the name number: " + (i+1));
            names.add(TextIO.getWord());

        }

        int randomNumber = Prac5.random(0, numberOfNames - 1);

        System.out.println(names.get(randomNumber));

    }
*/

}
