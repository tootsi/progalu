package Practicum6;

import Praktikum5.Prac5;
import lib.TextIO;

import java.util.ArrayList;

public class TenNamesImp {

    public static void main(String[] args) {
        randomName();
        //randomNameWithArrayList();


    }

    public static void randomName () {
        System.out.println("How many names: ");
        int numberOfNames = TextIO.getInt();

        String[] names = new String[numberOfNames];

        for (int i = 0; i < numberOfNames; i++) {
            System.out.println("Input a name" + (i+1));
            names[i] = TextIO.getWord();
        }

        //int debugPoint = 0;

        int randomNumber = Prac5.random(0, numberOfNames-1);

        System.out.println(names[randomNumber]);
    }


    //With Array:
    public static void randomNameWithArrayList() {
        System.out.println("How many names: ");
        int numberOfNames = TextIO.getInt();

        ArrayList<String> names = new ArrayList<>();

        for (int i = 0; i < numberOfNames; i++) {
            System.out.println("Please enter the name number: " + (i+1));
            names.add(TextIO.getWord());

        }

        int randomNumber = Prac5.random(0, numberOfNames - 1);

        System.out.println(names.get(randomNumber));

    }


}
