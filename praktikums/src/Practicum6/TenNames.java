package Practicum6;

import Praktikum5.Prac5;
import lib.TextIO;

import java.util.ArrayList;

public class TenNames {
    public static void main(String[] args) {
        randomNameFrom10();
        randomNameFrom10WithArrayList();



    }

    public static void randomNameFrom10 () {
        String[] names = new String[10];

        for (int i = 0; i < 10; i++) {
            System.out.println("Input a name" + (i+1));
            names[i] = TextIO.getln();
        }

        //int debugPoint = 0;

        int randomNumber = Prac5.random(0, 9);

        System.out.println(names[randomNumber]);
    }


    //With Array:
    public static void randomNameFrom10WithArrayList() {
        ArrayList<String> names = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            System.out.println("Please enter the name number" + (i+1));
            names.add(TextIO.getln());

        }

        int randomNumber = Prac5.random(0, 9);

        System.out.println(names.get(randomNumber));

    }


}