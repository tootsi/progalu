package Practicum6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class test63 {
    public static void main(String[] args) {

        arrayListDemo();

    }

    public static void arrayListDemo() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(4);
        numbers.add(8);
        numbers.add(5);
        numbers.add(6);

        System.out.println(numbers);

        //want to remove second element from the ..
        //second element index is 1, because it starts from 0

        numbers.remove(1);

        System.out.println(numbers);


        //replace 1st number with -10
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) == 4) {
                numbers.set(i, -10);
            }
        }

        System.out.println(numbers);

        Collections.sort(numbers);

        System.out.println(numbers);
    }

}
