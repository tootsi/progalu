package Practicum6;

import lib.TextIO;

import java.util.ArrayList;

public class NamesTillEmptyLine {

    public static void main(String[] args) {

        NumberOfAsInNames();

    }

    public static void NumberOfAsInNames() {
        ArrayList<String> names = new ArrayList<>();

        //since we don't know how many times the user will enter names, the loop will have to be "while"

        boolean isEmpty = false;
        while (!isEmpty) {
            System.out.println("Enter a name: ");
            String name = TextIO.getln();
            if (name.isEmpty()) {
                isEmpty = true;
            } else {
                names.add(name);
            }
        }

        for (String name : names) {
            int ammountOfAs = 0;
            for (int i = 0; i < name.length(); i++) {
                if (name.charAt(i) == 'a' || name.charAt(i) == 'A') {
                    ammountOfAs++;
                }
            }

            System.out.println(ammountOfAs + " " + name);

        }

        System.out.println(names);



    }

}
