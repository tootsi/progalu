package Practicum6;

import lib.TextIO;

public class ReverseNums2 {
    public static void main(String[] args) {
//Write a program that lets user insert 10 numbers and then prints those numbers in opposite order.

        reverse10NumbersTwo();

    }

    public static void reverse10NumbersTwo() {

        int[] numbers = new int[10];
        for (int i = 0; i < 10; i++) {
            System.out.println("Enter number: ");
            numbers[i] = TextIO.getInt();

        }

        int[] reversed = reverseNumbersTwo(numbers);

        for (int i = 0; i < reversed.length; i++) {
            System.out.println(reversed[i]);

        }

    }

    public static int[] reverseNumbersTwo(int[] arrayToReverseTwo) {

        int[] reversedArrayTwo = new int[arrayToReverseTwo.length]; // The size of the reversed array needs to be the same size as the original Array
        for (int i = 0; i < arrayToReverseTwo.length; i++) {
            reversedArrayTwo[arrayToReverseTwo.length - 1 - i] = arrayToReverseTwo[i]; //Take the value from the original array [arratToReverse[i]) -> Assign it to the reverse array

        }

        return reversedArrayTwo;
    }
}
