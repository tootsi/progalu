package Practicum8;

/**
 * Created by Ats on 15.11.2016.
 */
public enum Suite {

    DIAMONDS, CLUBS, SPADES, HEARTS;

    @Override
    public String toString() {
        char suite;
        switch (this) {
            case DIAMONDS:
                suite = '♦';
                break;
            case CLUBS:
                suite = '♣';
                break;
            case SPADES:
                suite = '♠';
                break;
            case HEARTS:
                suite = '♥';
                break;
            default:
                throw new IllegalStateException("Error" + super.toString());
        }
        return String.valueOf(suite);
    }

}
