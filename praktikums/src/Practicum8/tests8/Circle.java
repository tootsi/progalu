package Practicum8.tests8;

public class Circle extends Shape {
    double radius;


    @Override
    double area() {
        return radius + radius * Math.PI;
    }
}
