package Practicum8.tests8;

/**
 * Created by Ats on 15.11.2016.
 */
public enum Test83 {
    MONDYA(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6), SUNDAY(7);

    private int dayNumber;

    Test83(int num) {
        dayNumber = num;
    }
}
