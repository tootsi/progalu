package Practicum8.tests8;

import java.awt.*;

public abstract class Shape {
    Color color;
    abstract double area();
}
