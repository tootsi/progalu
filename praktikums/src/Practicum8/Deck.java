package Practicum8;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Ats on 15.11.2016.
 */
public class Deck {
    private ArrayList<Card> cards = new ArrayList<>();

    Deck () {
        for (Suite suite : Suite.values()) {
            for (Rank rank : Rank.values()) {
                Card card = new Card(rank, suite);
                cards.add(card);
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    public Card draw() {
        return cards.remove(0);
    }

}
