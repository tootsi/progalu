package Practicum8;

/**
 * Created by Ats on 15.11.2016.
 */
public class Card {

    private Rank rank;
    private Suite suite;

    public Card(Rank rank, Suite suite) {
        this.rank = rank;
        this.suite = suite;
    }

    @Override
    public String toString() {
        return String.format("%s of %s", rank, suite);
/*        return "Card{" +
                "rank=" + rank +
                ", suite=" + suite +
                '}';*/
    }
}
