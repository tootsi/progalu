package Praktikum5;


public class RanNum2 {
    public static void main(String[] args) {
        System.out.println(random(1, 5));

    }

    public static int random(int min, int max) {
        int range = max - min;
        int random = (int) (Math.random() * (range + 1)) + min;
        return random;
    }
}
