package Praktikum5;

import lib.TextIO;

public class ProblemCode {
    public static void main(String[] args) {
        //get positive integer value from user
        int input = getUserInput();
        //System.out.println("You entered the number: " + input); // Needst to be deleted
        //increase it by five
        int changedInput = increaseInputBy5(input);
        //System.out.println("This is the changed number: " + changedInput);  // Needst to be deleted
        //if it is > 10 then print out large number
        //if it is 10 then print out good number
        //if smaller than 10 then print out small number
        evaluateInput(changedInput);
    }

    private static void evaluateInput(int input) {
        if (input > 10) {
            System.out.println("That is a large number");
        } else if (input == 10) {
            System.out.println("This is a good number");
        } else {
            System.out.println("This is a small number");
        }

/*        if (input >= 10) {
            System.out.println("That is a large number");
        }
        if (input == 10) {
            System.out.println("That is a small number");
            if (input <= 10) {
                System.out.println("That is a good number");
            }
        }*/

    }

    private static int increaseInputBy5(int input) {
/*        int var1 = 4;*/
/*        int var2 = 6;*/
        return input + 5;
    }

    private static int getUserInput() {
        System.out.println("Enter a positive integer number");

        int number = TextIO.getInt();

        if (number > 0) {
            return number;
        } else {
            return 0;
        }
    }


}