package Praktikum5;

public class MethodDemo {
    public static void main(String[] args) {
        test();
        //void method
        saySomething("Hello", "you!");


    }
    static void test() {
        System.out.println("Hi!");
    }

    public static void saySomething(String firstPart, String secondPart) {
        System.out.println(firstPart + " " + secondPart);

    }
}
