package Praktikum5;


public class RandomNumber {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            // Math.random() gives a random number [0..1)
            // The number never reaches 1
            int random = (int) (Math.random() * 8) + 3;
            System.out.format("A random number from 3 to 10: %d\n", random);
        }

    }

}